<?php

namespace App\Console;

use App\Services\Search\Commands\CreateIndexCommand;
use App\Services\Search\Commands\ReindexBrand;
use App\Services\Search\Commands\ReindexCar;
use App\Services\Search\Commands\ReindexGeneric;
use App\Services\Search\Commands\ReindexProduct;
use App\Services\Search\Commands\UpdateBrand;
use App\Services\Search\Commands\UpdateGeneric;
use App\Services\Search\Commands\UpdateProduct;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CreateIndexCommand::class,
        // Reindex
        ReindexBrand::class,
        ReindexGeneric::class,
        ReindexProduct::class,
        ReindexCar::class,

        // Update
        UpdateBrand::class,
        UpdateGeneric::class,
        UpdateProduct::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  Schedule  $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
