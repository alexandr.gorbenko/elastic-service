<?php


namespace App\Http\Middleware;


use Closure;

class DebugMiddleware
{

    /**
     * @param $request
     * @param  Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('debug')) {
            config(['app.debug' => true]);
        }

        if (isDebugMode()) {
            $this->printDebugTitle();
        }

        return $next($request);
    }

    /**
     * Prints debug title
     */
    private function printDebugTitle(): void
    {
        echo '<div style="text-align: center">';
        echo '<div style="color: indianred; font-size: 30px">DEBUG MODE</div>';
        echo '<div>(set APP_DEBUG to "false" in your ENV file to turn off this mode)</div>';
        echo '</div>';
        echo '<hr>';
    }
}