<?php


namespace App\Http\Middleware;


use Closure;

class SearchMiddleware
{

    /**
     * @param \Illuminate\Http\Request $request
     * @param  Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($query = $request->get('query')) {
            $request->offsetSet('query', $this->modifyQuery($query));
        }

        return $next($request);
    }

    /**
     * @param  string  $query
     *
     * @return false|string|string[]
     */
    private function modifyQuery(string $query) {
        $query = trim($query);
        $query = mb_strtolower($query);

        return $query;
    }

}