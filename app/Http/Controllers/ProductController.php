<?php


namespace App\Http\Controllers;


use App\Services\Search\Facades\Search;

/**
 * @group Products
 *
 * Class ProductsController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * Search
     *
     * @bodyParam  token string required Access token. Example: test-atd/F5jg9hjsaq1Ywp2bYW7jccsow1QTspA1YTsFP6b2zxpkySGN2eJQwn4
     * @bodyParam  language string required Language code, size:2. Example: de
     * @bodyParam  query string required What to search. Example: bremsscheibe
     *
     * @responseFile responses/products_search.json
     *
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function search()
    {
        return $this->response(
            Search::product_search()
        );
    }
}
