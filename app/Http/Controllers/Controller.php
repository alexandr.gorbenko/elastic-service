<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

    public function __construct(Request $request)
    {
        $this->validate($request, $this->validationRules());
    }


    /**
     * @return array
     */
    protected function validationRules()
    {
        return [];
    }


    /**
     * @param  array  $content
     *
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function response(array $content)
    {
        if (!empty($content['debug'])) {
            return '';
        }

        return response()->json($content);
    }

}
