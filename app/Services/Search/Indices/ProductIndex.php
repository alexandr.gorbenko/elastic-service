<?php


namespace App\Services\Search\Indices;


use App\Services\Search\Contracts\DataProviderContract;
use App\Services\Search\Contracts\MappingContract;
use App\Services\Search\DataProviders\ProductDataProvider;
use App\Services\Search\Entities\Product;
use App\Services\Search\Index;
use App\Services\Search\Mappings\ProductMapping;

class ProductIndex extends Index
{
    public $threadCount = 8;

    /**
     * Index name
     *
     * @return string
     */
    public function index(): string
    {
        return Product::_INDEX_;
    }

    /**
     * Mapping
     *
     * @return MappingContract
     */
    public function mapping(): MappingContract
    {
        return new ProductMapping();
    }

    /**
     * Data Provider
     *
     * @return DataProviderContract
     */
    public function dataProvider(): DataProviderContract
    {
        return new ProductDataProvider();
    }
}
