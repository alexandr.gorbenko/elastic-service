<?php


namespace App\Services\Search\Commands;


use App\Services\Search\Indices\ProductIndex;
use Illuminate\Console\Command;

class UpdateProduct extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = "Update products index";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create, update and delete docs in products index to synchronize with database';

    /**
     * @var string command
     */
    protected $signature = 'search:update-products';

    /**
     * Handle the command
     */
    public function handle()
    {
        (new ProductIndex($this->getOutput()))->update();
    }
}
