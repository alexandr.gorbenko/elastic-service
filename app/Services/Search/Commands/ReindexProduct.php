<?php


namespace App\Services\Search\Commands;


use App\Services\Search\Command;
use App\Services\Search\Indices\ProductIndex;

class ReindexProduct extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = "Reindex Products";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new products index and update alias';

    /**
     * @var string command
     */
    protected $signature = 'search:reindex-product {thread}';

    /**
     * Handle the command
     */
    public function handle()
    {
        (new ProductIndex($this->output, $this->argument('thread')))->reindex();
    }
}
