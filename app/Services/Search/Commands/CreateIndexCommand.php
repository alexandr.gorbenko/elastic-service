<?php


namespace App\Services\Search\Commands;


use App\Services\Search\Command;
use App\Services\Search\Entities\Product;
use App\Services\Search\Exceptions\WrongIndexType;
use App\Services\Search\Index;
use App\Services\Search\Indices\ProductIndex;

class CreateIndexCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = "Create index";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new index and returns its name';

    /**
     * @var string command
     */
    protected $signature = 'search:create-index {type} {--force}';

    /**
     * Handle the command
     */
    public function handle()
    {
        $index = $this->getIndex($this->argument('type'));

        if ($index->hasActive() && empty($this->option('force'))) {
            $this->output->error('This type already has active index. You can use "--force" option to create index anyway.');
            exit;
        }

        $index->create();
    }


    public function getIndex(string $type): Index
    {
        $indices = [
            Brand::_TYPE_ => BrandIndex::class,
            Category::_TYPE_ => GenericIndex::class,
            Product::_TYPE_ => ProductIndex::class,
            Car::_TYPE_ => CarIndex::class,
        ];

        foreach ($indices as $name => $class) {
            if ($name == $type) {
                return new $class($this->output);
            }
        }

        throw new WrongIndexType('Unknown type: '.$type);
    }
}