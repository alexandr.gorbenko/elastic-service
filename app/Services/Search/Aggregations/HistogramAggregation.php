<?php


namespace App\Services\Search\Aggregations;


use App\Services\Search\Aggregation;

class HistogramAggregation extends Aggregation
{
    /**
     * @var string
     */
    protected $field;
    /**
     * @var int
     */
    protected $interval;

    /**
     * HistogramAggregation constructor.
     *
     * @param  string  $name
     * @param  string  $field
     * @param  int  $interval
     */
    public function __construct(string $name, string $field, int $interval)
    {
        $this->field = $field;
        $this->interval = $interval;

        parent::__construct($name);
    }

    /**
     * @return array[]
     */
    public function toArray(): array
    {
        return [
            'histogram' => [
                'field' => $this->field,
                'interval' => $this->interval
            ]

        ];
    }
}