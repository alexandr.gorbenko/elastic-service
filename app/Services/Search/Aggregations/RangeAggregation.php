<?php


namespace App\Services\Search\Aggregations;


use App\Services\Search\Aggregation;

class RangeAggregation extends Aggregation
{
    /**
     * @var string
     */
    protected $field;
    /**
     * @var array
     */
    protected $ranges;

    public function __construct(string $name, string $field, array $ranges)
    {
        $this->field = $field;
        $this->ranges = $this->getRanges($ranges);

        parent::__construct($name);
    }

    protected function getRanges(array $data): array
    {
        $ranges = [];
        foreach ($data as $from => $to) {
            $range = [];
            if (isset($from) && $from > 0) {
                $range['from'] = $from;
            }
            if (isset($to) && $to !== true) {
                $range['to'] = $to;
            }

            if (!empty($range)) {
                $ranges[] = $range;
            }
        }

        return $ranges;
    }

    public function toArray(): array
    {
        return [
            'range' => [
                'field' => $this->field,
                'ranges' => $this->ranges,
            ]
        ];
    }


}