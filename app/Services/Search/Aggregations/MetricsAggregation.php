<?php


namespace App\Services\Search\Aggregations;


use App\Services\Search\Aggregation;

class MetricsAggregation extends Aggregation
{

    const TYPE_MIN = 'min';
    const TYPE_MAX = 'max';
    const TYPE_AVG = 'avg';
    const TYPE_SUM = 'sum';
    const TYPE_STATS = 'stats';

    private $field;
    private $type;

    public function __construct(string $name, string $field, string $type)
    {
        $this->field = $field;
        $this->type = $type;

        parent::__construct($name);
    }

    public function toArray(): array
    {
        return [
            $this->type => [
                'field' => $this->field
            ]
        ];
    }


}