<?php


namespace App\Services\Search\Aggregations\SearchProperty;


use App\Services\Search\Aggregations\TermsAggregation;
use App\Services\Search\Contracts\AggregationContract;
use App\Services\Search\Enums\SearchProperty;
use App\Services\Search\Enums\SortOrder;
use App\Services\Search\Expression;

class SearchWeightAggregation extends TermsAggregation
{
    public function __construct(AggregationContract $aggregation = null)
    {
        parent::__construct(
            searchProperty(SearchProperty::WEIGHT),
            searchProperty(SearchProperty::WEIGHT),
            self::DEFAULT,
            0,
            new Expression(['_term' => SortOrder::DESC]),
            $aggregation
        );
    }


}