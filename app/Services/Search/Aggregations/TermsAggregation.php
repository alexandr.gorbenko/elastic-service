<?php


namespace App\Services\Search\Aggregations;


use App\Services\Search\Aggregation;
use App\Services\Search\Contracts\AggregationContract;
use App\Services\Search\Contracts\ExpressionContract;

class TermsAggregation extends Aggregation
{

    /**
     * @var string
     */
    protected $field;
    /**
     * @var string
     */
    protected $term;
    /**
     * @var int
     */
    protected $size;
    /**
     * @var ExpressionContract
     */
    protected $order;
    /**
     * @var AggregationContract
     */
    protected $aggregation;


    public function __construct(
        string $name,
        string $field,
        int $size = 0,
        ExpressionContract $order = null,
        AggregationContract $aggregation = null
    ) {
        $this->field = $field;
        $this->size = $size;
        $this->order = $order;
        $this->aggregation = $aggregation;

        parent::__construct($name);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $term['field'] = $this->field;

        if ($this->size > 0) {
            $term['size'] = $this->size;
        } else {
            $term['size'] = self::MAX_SIZE;
        }

        if ($this->order) {
            $term['order'] = getValue($this->order);
        }


        $aggregation['terms'] = $term;

        if ($this->aggregation) {
            $aggregation['aggs'] = getValue($this->aggregation);
        }

        return $aggregation;
    }


}