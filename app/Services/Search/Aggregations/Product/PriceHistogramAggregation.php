<?php


namespace App\Services\Search\Aggregations\Product;


use App\Services\Search\Aggregations\HistogramAggregation;
use App\Services\Search\Entities\Product;

class PriceHistogramAggregation extends HistogramAggregation
{
    /**
     * PriceHistogramAggregation constructor.
     *
     * @param  string  $name
     * @param  int  $interval
     */
    public function __construct(string $name = Product::PRICE, int $interval = 50)
    {
        parent::__construct($name, Product::PRICE, $interval);
    }


}