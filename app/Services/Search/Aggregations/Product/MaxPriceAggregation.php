<?php


namespace App\Services\Search\Aggregations\Product;


use App\Services\Search\Aggregations\MetricsAggregation;
use App\Services\Search\Entities\Product;

class MaxPriceAggregation extends MetricsAggregation
{
    public function __construct(string $name)
    {
        parent::__construct($name, Product::PRICE, self::TYPE_MAX);
    }


}