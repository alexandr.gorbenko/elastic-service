<?php


namespace App\Services\Search\Enums;


use App\Services\Search\Exceptions\MappingException;
use App\Services\Search\Mapping;

class SearchProperty
{

    /**
     * Property for search enable. Can be 1 or 0
     */
    const ENABLE = 'enable';
    /**
     * Property for search weights. Can be from 0 to 2
     */
    const WEIGHT = 'weight';

    /**
     * @var string
     */
    protected static $groupName = 'search_property';

    /**
     * @var array[]
     */
    protected static $searchMappings = [
        self::ENABLE => [
            'type' => Mapping::TYPE_BOOLEAN,
        ],
        self::WEIGHT => [
            'type' => Mapping::TYPE_DOUBLE,
        ],
    ];

    /**
     * @param  string  $propertyName
     *
     * @return string
     */
    public static function getField(string $propertyName): string
    {
        return field(static::$groupName, $propertyName);
    }

    /**
     * @param  array  $mapping
     * @param  array  $searchProperties
     *
     * @throws MappingException
     */
    public static function populateMapping(array &$mapping, array $searchProperties): void
    {
        if (empty($searchProperties)) {
            return;
        }

        $duplicates = array_intersect(array_keys($mapping), $searchProperties);
        if ($duplicates) {
            $duplicates = implode(', ', $duplicates);
            throw new MappingException("Fields $duplicates is already taken for default search properties. Choose another names");
        }

        $result = [];

        foreach (static::$searchMappings as $field => $properties) {
            if (in_array($field, $searchProperties)) {
                $result[$field] = $properties;
            }
        }

        $mapping[static::$groupName] = ['properties' => $result];
    }

    /**
     * @param  array  $document
     * @param  array  $searchProperties
     */
    public static function populateDocument(array &$document, array $searchProperties): void
    {
        if (empty($searchProperties)) {
            return;
        }

        if (empty($document[static::$groupName])) {
            $document[static::$groupName] = [];
        }

        $properties = [
            self::ENABLE => function ($value) {
                return (bool)($value ?? true);
            },
            self::WEIGHT => function ($value) {
                return transformValue($value ?? 0, -100, 100, 0, 2);
            }
        ];

        foreach ($properties as $name => $closure) {
            if (in_array($name, $searchProperties)) {
                $key = static::getField($name);
                $value = $document[$key] ?? null;

                $document[static::$groupName][$name] = $closure($value);

                unset($document[$key]);
            }
        }
    }

    public static function getGroupName(): string
    {
        return static::$groupName;
    }
}