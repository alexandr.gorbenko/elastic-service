<?php


namespace App\Services\Search\Enums;


class SortOrder
{
    /**
     * Sorting from smaller to larger
     */
    const ASC = 'asc';
    /**
     * Sorting from larger to smaller
     */
    const DESC = 'desc';

}