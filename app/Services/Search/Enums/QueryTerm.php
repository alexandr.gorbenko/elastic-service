<?php


namespace App\Services\Search\Enums;


class QueryTerm
{
    /**
     * Items must contain term values
     */
    const MUST = 'must';
    /**
     * Items must not contain term values
     */
    const MUST_NOT = 'must_not';
    /**
     * Items with term values will got larger priority
     */
    const SHOULD = 'should';
    /**
     * Filter context
     */
    const FILTER = 'filter';

}