<?php


namespace App\Services\Search;


use App\Services\Search\Contracts\MappingContract;
use App\Services\Search\Enums\SearchProperty;
use App\Services\Search\Exceptions\MappingException;

abstract class Mapping implements MappingContract
{
    const TYPE_INTEGER = 'integer';
    const TYPE_DOUBLE = 'double';
    const TYPE_TEXT = 'text';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_NESTED = 'nested';

    /**
     * Default analyzer. Good for most type of situations
     */
    const ANALYZER_DEFAULT = 'default';
    /**
     * Keyword analyzer. Good for analyzing one-word fields like name or sequence of numbers
     */
    const ANALYZER_KEYWORD = 'keyword';
    /**
     * Clean text analyzer. Almost like default analyzer, but removes all the special symbols from text
     */
    const ANALYZER_CLEAN_TEXT = 'clean_text';
    /**
     * Words only analyzer. Almost like default analyzer, but leaves only words and whitespaces
     */
    const ANALYZER_WORDS_ONLY = 'words_only';
    /**
     * Numbers only analyzer. Leaves only numbers and whitespaces
     */
    const ANALYZER_NUMBERS_ONLY = 'numbers_only';
    /**
     * Autocomplete analyzer. Uses edge ngram logic, good for autocomplete usage
     */
    const ANALYZER_AUTOCOMPLETE = 'autocomplete';

    /**
     * Id field
     */
    const FIELD_ID = '_id';
    /**
     * Source field name
     */
    const FIELD_SOURCE = '_source';
    /**
     * Score field name
     */
    const FIELD_SCORE = '_score';
    /**
     * Highlight field name
     */
    const FIELD_HIGHLIGHT = 'highlight';

    /**
     * Autocomplete suffix name
     */
    const FIELD_AUTOCOMPLETE = 'autocomplete';

    /**
     * Array of additional search properties
     *
     * @var array
     */
    public $searchProperties = [];


    /**
     * Mapping body
     *
     * @return array
     * @throws MappingException
     */
    public function body(): array
    {
        return [
            'settings' => $this->settings(),
            'mappings' => [
                'properties' => $this->properties()
            ]
        ];
    }

    /**
     * Mapping settings
     *
     * @return array
     */
    protected function settings(): array
    {
        return [
            'analysis' => [
                'filter' => $this->filters(),
                'char_filter' => $this->charFilters(),
                'tokenizer' => $this->tokenizers(),
                'analyzer' => $this->analyzers(),
            ]
        ];
    }

    /**
     * Mapping filters
     *
     * @return array
     */
    protected function filters(): array
    {
        return [
            'german_stemmer' => ['type' => 'stemmer', 'language' => 'german'],
            'german_stop' => ['type' => 'stop', 'stopwords' => '_german_'],
            'english_stemmer' => ['type' => 'stemmer', 'language' => 'english'],
            'english_stop' => ['type' => 'stop', 'stopwords' => '_english_'],
            'spanish_stemmer' => ['type' => 'stemmer', 'language' => 'light_spanish'],
            'spanish_stop' => ['type' => 'stop', 'stopwords' => '_spanish_'],
            'french_stemmer' => ['type' => 'stemmer', 'language' => 'light_french'],
            'french_stop' => ['type' => 'stop', 'stopwords' => '_french_'],
            'italian_stemmer' => ['type' => 'stemmer', 'language' => 'light_italian'],
            'italian_stop' => ['type' => 'stop', 'stopwords' => '_italian_'],
            'danish_stemmer' => ['type' => 'stemmer', 'language' => 'danish'],
            'danish_stop' => ['type' => 'stop', 'stopwords' => '_danish_'],
            'finnish_stemmer' => ['type' => 'stemmer', 'language' => 'finnish'],
            'finnish_stop' => ['type' => 'stop', 'stopwords' => '_finnish_'],
            'greek_stemmer' => ['type' => 'stemmer', 'language' => 'greek'],
            'greek_stop' => ['type' => 'stop', 'stopwords' => '_greek_'],
            'hungarian_stemmer' => ['type' => 'stemmer', 'language' => 'hungarian'],
            'hungarian_stop' => ['type' => 'stop', 'stopwords' => '_hungarian_'],
            'dutch_stemmer' => ['type' => 'stemmer', 'language' => 'dutch'],
            'dutch_stop' => ['type' => 'stop', 'stopwords' => '_dutch_'],
            'norwegian_stemmer' => ['type' => 'stemmer', 'language' => 'norwegian'],
            'norwegian_stop' => ['type' => 'stop', 'stopwords' => '_norwegian_'],
            'portuguese_stemmer' => ['type' => 'stemmer', 'language' => 'light_portuguese'],
            'portuguese_stop' => ['type' => 'stop', 'stopwords' => '_portuguese_'],
            'romanian_stemmer' => ['type' => 'stemmer', 'language' => 'romanian'],
            'romanian_stop' => ['type' => 'stop', 'stopwords' => '_romanian_'],
            'swedish_stemmer' => ['type' => 'stemmer', 'language' => 'swedish'],
            'swedish_stop' => ['type' => 'stop', 'stopwords' => '_swedish_'],
            'czech_stemmer' => ['type' => 'stemmer', 'language' => 'czech'],
            'czech_stop' => ['type' => 'stop', 'stopwords' => '_czech_'],
            'bulgarian_stemmer' => ['type' => 'stemmer', 'language' => 'bulgarian'],
            'bulgarian_stop' => ['type' => 'stop', 'stopwords' => '_bulgarian_'],
            'russian_stemmer' => ['type' => 'stemmer', 'language' => 'russian'],
            'russian_stop' => ['type' => 'stop', 'stopwords' => '_russian_'],

//            'lithuanian_stemmer' => ['type' => 'stemmer', 'language' => 'lithuanian'],
            // 'lithuanian_stop'    => ['type' => 'stop', 'stopwords' => '_lithuanian_'],
//            'latvian_stemmer'    => ['type' => 'stemmer', 'language' => 'latvian'],
//            'latvian_stop'       => ['type' => 'stop', 'stopwords' => '_latvian_'],
//            'pl_hunspell'        => ["type" => "hunspell", "locale" => "pl_PL"],
//            'sk_hunspell'        => ["type" => "hunspell", "locale" => "sk_SK"],
//            'si_hunspell'        => ["type" => "hunspell", "locale" => "sl_SI"],
//            'lt_hunspell'        => ["type" => "hunspell", "locale" => "lt_LT"],
//            'ee_hunspell'        => ["type" => "hunspell", "locale" => "et_EE"],
        ];
    }

    /**
     * Mapping char filters
     *
     * @return array
     */
    protected function charFilters(): array
    {
        return [
            'clean_text_filter' => [
                'type' => 'pattern_replace',
                'pattern' => '([^\\pL0-9\s]+)',
                'replacement' => ''
            ],
            'words_only_filter' => [
                'type' => 'pattern_replace',
                'pattern' => '([^\\pL\s]+)',
                'replacement' => ''
            ],
            'numbers_only_filter' => [
                'type' => 'pattern_replace',
                'pattern' => '([^0-9\s]+)',
                'replacement' => ''
            ]
        ];
    }

    /**
     * Mapping tokenizers
     *
     * @return array
     */
    protected function tokenizers(): array
    {
        return [
            'autocomplete_tokenizer' => [
                'type' => 'edge_ngram',
                'min_gram' => 1,
                'max_gram' => 20,
                'token_chars' => [
                    'letter',
                    'digit'
                ]

            ]
        ];
    }

    /**
     * Mapping analyzers
     *
     * @return array
     */
    protected function analyzers(): array
    {
        $analyzers = [];

        $analyzers[self::ANALYZER_DEFAULT] = [
            'filter' => ["lowercase"],
            'type' => 'custom',
            'tokenizer' => 'standard'
        ];
        $analyzers[self::ANALYZER_KEYWORD] = [
            'filter' => ["lowercase"],
            'type' => 'custom',
            'tokenizer' => 'keyword'
        ];
        $analyzers[self::ANALYZER_CLEAN_TEXT] = [
            'filter' => ["lowercase"],
            'type' => 'custom',
            'tokenizer' => 'standard',
            'char_filter' => 'clean_text_filter'
        ];
        $analyzers[self::ANALYZER_WORDS_ONLY] = [
            'filter' => ["lowercase"],
            'type' => 'custom',
            'tokenizer' => 'standard',
            'char_filter' => 'words_only_filter'
        ];
        $analyzers[self::ANALYZER_NUMBERS_ONLY] = [
            'filter' => ["lowercase"],
            'type' => 'custom',
            'tokenizer' => 'standard',
            'char_filter' => 'numbers_only_filter'
        ];
        $analyzers[self::ANALYZER_AUTOCOMPLETE] = [
            'filter' => ["lowercase"],
            'type' => 'custom',
            'tokenizer' => 'autocomplete_tokenizer',
            'char_filter' => 'clean_text_filter'
        ];

        foreach ($this->languagesMap() as $lang => $settings) {
            $analyzers[$lang.'_'.self::ANALYZER_DEFAULT] = $settings;
            $analyzers[$lang.'_'.self::ANALYZER_CLEAN_TEXT] = array_replace($settings,
                ['char_filter' => 'clean_text_filter']);
            $analyzers[$lang.'_'.self::ANALYZER_WORDS_ONLY] = array_replace($settings,
                ['char_filter' => 'words_only_filter']);
            $analyzers[$lang.'_'.self::ANALYZER_AUTOCOMPLETE] = array_replace($settings,
                ['tokenizer' => 'autocomplete_tokenizer', 'char_filter' => 'clean_text_filter']);
        }

        return $analyzers;
    }

    /**
     * Array of default analyzer settings
     *
     * @return array
     */
    protected function languagesMap(): array
    {
        return [
            'de' => [
                'filter' => ["lowercase", "german_stop", "german_normalization", "german_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'en' => [
                'filter' => ["lowercase", "english_stop", "english_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'es' => [
                'filter' => ["lowercase", "spanish_stop", "german_normalization", "spanish_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'fr' => [
                'filter' => ["lowercase", "french_stop", "german_normalization", "french_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'it' => [
                'filter' => ["lowercase", "italian_stop", "german_normalization", "italian_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'dk' => [
                'filter' => ["lowercase", "danish_stop", "scandinavian_normalization", "danish_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'fi' => [
                'filter' => ["lowercase", "finnish_stop", "scandinavian_normalization", "finnish_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'no' => [
                'filter' => ["lowercase", "norwegian_stop", "scandinavian_normalization", "norwegian_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'se' => [
                'filter' => ["lowercase", "swedish_stop", "scandinavian_normalization", "swedish_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'gr' => [
                'filter' => ["lowercase", "greek_stop", "greek_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'hu' => [
                'filter' => ["lowercase", "hungarian_stop", "hungarian_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'nl' => [
                'filter' => ["lowercase", "dutch_stop", "dutch_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'pt' => [
                'filter' => ["lowercase", "portuguese_stop", "portuguese_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'ro' => [
                'filter' => ["lowercase", "romanian_stop", "romanian_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'cz' => [
                'filter' => ["lowercase", "czech_stop", "czech_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],
            'bg' => [
                'filter' => ["lowercase", "bulgarian_stop", "bulgarian_stemmer"],
                'type' => 'custom',
                'tokenizer' => 'standard'
            ],

//            'lv'     => ['filter' => ["lowercase", "latvian_stop", "latvian_stemmer"], 'type' => 'custom', 'tokenizer' => 'standard'],
//            'lt'     => ['filter' => ["lowercase", "lithuanian_stemmer", "lt_hunspell"], 'type' => 'custom', 'tokenizer' => 'standard'],
//            'pl'     => ['filter' => ["lowercase", 'pl_hunspell'], 'type' => 'custom', 'tokenizer' => 'standard'],
//            'sk'     => ['filter' => ["lowercase", 'sk_hunspell'], 'type' => 'custom', 'tokenizer' => 'standard'],
//            'si'     => ['filter' => ["lowercase", 'si_hunspell'], 'type' => 'custom', 'tokenizer' => 'standard'],
//            'ee'     => ['filter' => ["lowercase", 'ee_hunspell'], 'type' => 'custom', 'tokenizer' => 'standard'],
        ];
    }

    /**
     * Mapping properties
     *
     * @return array
     * @throws MappingException
     */
    protected function properties()
    {
        $mapping = $this->mapping();

        SearchProperty::populateMapping($mapping, $this->searchProperties);

        return $mapping;
    }
}
