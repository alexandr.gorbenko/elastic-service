<?php


namespace App\Services\Search;


class Paginator
{
    /**
     * @var array
     */
    protected $items;
    /**
     * @var int
     */
    protected $total;
    /**
     * @var string
     */
    protected $relation;
    /**
     * @var int
     */
    protected $perPage;
    /**
     * @var int
     */
    protected $currentPage;
    /**
     * @var bool
     */
    protected $hasNextPage;
    /**
     * @var bool
     */
    protected $hasPreviousPage;
    /**
     * @var int
     */
    protected $lastPage;
    /**
     * @var array
     */
    protected $options;

    /**
     * Paginator constructor.
     *
     * @param  array  $items
     * @param  int  $total
     * @param  string  $relation
     * @param  int  $perPage
     * @param  int  $currentPage
     * @param  array  $options
     */
    public function __construct(
        array $items,
        int $total,
        string $relation,
        int $perPage,
        int $currentPage = 1,
        $options = []
    ) {
        $this->items = $items;
        $this->total = $total;
        $this->relation = $relation;
        $this->perPage = $perPage;
        $this->currentPage = $currentPage;
        $this->options = $options;

        $this->calculateLastPage();

        $this->resolveNextPage();
        $this->resolvePreviousPage();
    }

    protected function resolveNextPage(): void
    {
        $this->hasNextPage = $this->currentPage < $this->lastPage;
    }


    protected function resolvePreviousPage(): void
    {
        $this->hasPreviousPage = $this->currentPage > 1;
    }


    protected function calculateLastPage(): void
    {
        if ($this->total > 0 && $this->perPage > 0) {
            $this->lastPage = ceil($this->total / $this->perPage);
        } else {
            $this->lastPage = 1;
        }
    }

    /**
     * Get array of items
     *
     * @return array
     */
    public function getItems(): array
    {
        if (count($this->items) > $this->perPage) {
            $startIndex = ($this->currentPage - 1) * $this->perPage;
            $this->items = array_slice($this->items, $startIndex, $this->perPage);
        }

        return $this->items;
    }

    /**
     * Get count of items
     *
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return string
     */
    public function getRelation(): string
    {
        return $this->relation;
    }

    /**
     * Get count of items per page
     *
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * Get index of current page
     *
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * Checks if exists next page
     *
     * @return bool
     */
    public function isHasNextPage(): bool
    {
        return $this->hasNextPage;
    }

    /**
     * Checks if exists previous page
     *
     * @return bool
     */
    public function isHasPreviousPage(): bool
    {
        return $this->hasPreviousPage;
    }

    /**
     * Get index of last page
     *
     * @return int
     */
    public function getLastPage(): int
    {
        return $this->lastPage;
    }

    /**
     * Get options data
     *
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * Get map of paginator metadata
     *
     * @return array
     */
    public function getMetadata(): array
    {
        return [
            'total' => $this->getTotal(),
            'relation' => $this->getRelation(),
            'perPage' => $this->getPerPage(),
            'currentPage' => $this->getCurrentPage(),
            'hasNextPage' => $this->isHasNextPage(),
            'hasPreviousPage' => $this->isHasPreviousPage(),
            'lastPage' => $this->getLastPage()
        ];
    }

    /**
     * Format all paginator data to array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'items' => $this->getItems(),
            'paginator' => $this->getMetadata()
        ];
    }
}
