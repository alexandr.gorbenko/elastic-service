<?php


namespace App\Services\Search\Clients;


use App\Services\Search\Client as SearchClient;
use App\Services\Search\Requests\Product\ProductSearchRequest;

class TestClient extends SearchClient
{

    public function product_search()
    {
        return $this->execute(new ProductSearchRequest());
    }


}
