<?php


namespace App\Services\Search;


use App\Services\Search\Contracts\QueryContract;
use App\Services\Search\Exceptions\QueryParametersException;
use App\Services\Search\Traits\TParameters;
use App\Services\Search\Traits\TQueryBuilder;
use App\Services\Search\Traits\TQueryMacros;

abstract class Query implements QueryContract
{
    use TParameters, TQueryBuilder, TQueryMacros;

    /**
     * Init method
     */
    public function init(): void
    {
    }

    /**
     * Check query parameters rules
     */
    protected function checkParams(): void
    {
        foreach ($this->required() as $required) {
            if (!array_key_exists($required, $this->parameters) || !isset($this->parameters[$required])) {
                $missed[] = $required;
            }
        }

        if (!empty($missed)) {
            throw new QueryParametersException(
                "Missing parameters: ".implode(', ', $missed)
            );
        }
    }

    /**
     * Compile query
     *
     * @return array
     */
    public function compile(): array
    {
        $this->checkParams();

        return $this->toArray();
    }

    /**
     * Query body
     *
     * @return array
     */
    abstract protected function toArray(): array;
}
