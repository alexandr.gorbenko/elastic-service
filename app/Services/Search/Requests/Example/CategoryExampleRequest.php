<?php


namespace App\Services\Search\Requests\Example;


use App\Services\Search\Aggregations\SearchProperty\SearchWeightAggregation;
use App\Services\Search\Aggregations\TermsAggregation;
use App\Services\Search\Contracts\ResponseContract;
use App\Services\Search\Entities\Category;
use App\Services\Search\Enums\SearchProperty;
use App\Services\Search\Enums\SortOrder;
use App\Services\Search\Expression;
use App\Services\Search\Expressions\SearchProperty\SearchEnableTermExpression;
use App\Services\Search\Helpers\Params;
use App\Services\Search\Queries\MultiMatchQuery;
use App\Services\Search\Requests\SingleRequest;
use App\Services\Search\Responses\Generic\GenericResponse;
use App\Services\Search\Sort;
use App\Services\Search\Sorts\FieldSort;
use App\Services\Search\Sorts\MappingProperty\SearchWeightSort;

class CategoryExampleRequest extends SingleRequest
{
    /**
     * Get response class name
     *
     * @return ResponseContract
     */
    public function response(): ResponseContract
    {
        // Changing default response class to custom, now we can change response behavior
        return new GenericResponse();
    }

    /**
     * Index name
     *
     * @return string
     */
    public function index(): string
    {
        // Index to search in
        return Category::_INDEX_;
    }

    /**
     * Declare validation rules
     *
     * @return array
     */
    public function validationRules(): array
    {
        return [
            'lang' => 'required|string',
        ];
    }

    /**
     * Initialize method
     */
    public function init(): void
    {
        /**
         * Source fields
         */
        // field() function implode all its args with a dot, ex: field("aaa","bbb","ccc") => "aaa.bbb.ccc"
        $this->source(
            field(Category::NAME, $this->get('lang')), // genericArticleId.de
            field(Category::SYNONYMS, $this->get('lang')) // synonyms.de
        );

        // If you don't want to get data, just set source to "false"
        $this->source(false);

        // Setting size
        // we took size from http-query parameter as priority or default size for generics autocomplete from
        // config file, @see config/services
        $this->size($this->get('size', Params::autocompleteSize(Category::_TYPE_)));


        /**
         * Sorting (all methods return same result)
         */
        // Simple field sort behavior
        $this->sort(new FieldSort(SearchProperty::WEIGHT, SortOrder::DESC));
        // Named field sort behavior
        $this->sort(new SearchWeightSort());
        // Base sort with expression
        $this->sort(new Sort(new Expression([
            SearchProperty::WEIGHT => [
                'order' => SortOrder::DESC
            ]
        ])));


        // Setting simple terms aggregation
        $this->aggregation(new TermsAggregation('ids', Category::ID));
        // Setting custom aggregation
        $this->aggregation(new SearchWeightAggregation());
    }


    /**
     * Request query
     *
     * @return array
     */
    public function query(): array
    {
        $lang = $this->get('lang');

        return $this->createQuery(new MultiMatchQuery())
            ->fields( // Fields to search
                autocomplete(Category::NAME, $lang), // genericArticleId.de.autocomplete
                autocomplete(Category::SYNONYMS, $lang) // synonyms.de.autocomplete
            )
            ->operator('and')
            ->fuzziness('auto')
            ->whereMust(new SearchEnableTermExpression())
            ->compile();
    }

}
