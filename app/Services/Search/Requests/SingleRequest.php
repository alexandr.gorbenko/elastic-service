<?php


namespace App\Services\Search\Requests;


use App\Services\Search\Contracts\AggregationContract;
use App\Services\Search\Contracts\ExpressionContract;
use App\Services\Search\Contracts\ResponseContract;
use App\Services\Search\Contracts\SortContract;
use App\Services\Search\Facades\Search;
use App\Services\Search\Helpers\Memory;
use App\Services\Search\Request;
use App\Services\Search\Response;
use App\Services\Search\Responses\SingleResponse;
use App\Services\Search\Traits\TQueryBuilder;
use Illuminate\Support\Facades\Validator;
use stdClass;

abstract class SingleRequest extends Request
{
    use TQueryBuilder;

    /**
     * @var array
     */
    private $result;
    /**
     * @var int
     */
    private $page = 1;
    /**
     * @var array
     */
    private $metadata;
    /**
     * @var int|bool
     */
    protected $trackTotalHits = 10000;
    /**
     * @var int
     */
    protected $defaultSize = 10;
    /**
     * @var int
     */
    protected $defaultAggregationsSize = 10000;
    /**
     * @var array
     */
    private $aggregations;
    /**
     * @var array
     */
    private $sort;
    /**
     * @var array
     */
    private $highlight;
    /**
     * @var array
     */
    private $highlightTags = ['<em>', '</em>'];

    /**
     * Execute request and get response
     *
     * @return ResponseContract
     */
    public function execute(): ResponseContract
    {
        $validator = Validator::make($this->parameters, $this->validationRules(), config('validator.messages', []));

        if ($validator->fails()) {
            return $this->createResponse(Response::TYPE_ERROR,
                [
                    'code' => Response::CODE_ERROR_VALIDATION,
                    'message' => 'Validation fails',
                    'errors' => $validator->getMessageBag()->getMessages()
                ]
            );
        }

        $time = microtime(true);

        $this->result = Memory::remember(Memory::key($this, $this->parameters), function () {
            return Search::engine()->search([
                'index' => getIndexName($this->get('_index', $this->index())),
                'body' => $this->body()
            ]);
        });

        if (isDebugMode()) {
            $this->debugExecutionTime($time);
        }

        return $this->createResponse(Response::TYPE_SUCCESS);
    }

    /**
     * Get request body
     *
     * @return array
     */
    protected function body(): array
    {
        $body = [
            'track_total_hits' => $this->getTrackTotalHits(),
            'query' => $this->query(),
            'size' => $this->getSize(),
            '_source' => $this->getSource(),
            'from' => $this->getFrom(),
            'sort' => $this->allowEmpty($this->getSort()),
            'aggs' => $this->allowEmpty($this->getAggregations()),
            'highlight' => $this->allowEmpty($this->getHighlight()),
        ];

        if (isDebugMode()) {
            $this->debug($body);
        }

        return $body;
    }

    /**
     * Get array of results after request execution
     *
     * @return array
     */
    public function getResult(): ?array
    {
        return $this->result;
    }

    /**
     * Set "page" parameter
     *
     * @param  int  $page
     *
     * @return $this
     */
    public function page(?int $page): self
    {
        $this->page = max(1, $page);

        return $this;
    }

    /**
     * Get "page" parameter
     *
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * Get "from" parameter
     *
     * @return int
     */
    protected function getFrom(): int
    {
        return ($this->getPage() - 1) * $this->getSize();
    }

    /**
     * Set "track_total_hits" parameter.
     * Can be:
     * 1. (int) value - limit max count of docs
     * 2. (bool) true - count all docs
     * 3. (bool) false - turn off tracking docs count
     *
     * @param $trackTotalHits
     *
     * @return $this|Request
     */
    public function trackTotalHits($trackTotalHits): self
    {
        $this->trackTotalHits = $trackTotalHits;

        return $this;
    }

    /**
     * Get "track_total_hits" parameter
     *
     * @return bool|int
     */
    public function getTrackTotalHits()
    {
        return $this->trackTotalHits;
    }

    /**
     * Set "size" parameter
     *
     * @param  int  $size
     *
     * @return $this|Request
     */
    public function size(int $size): self
    {
        return $this->set('size', $size);
    }

    /**
     * Get "size" parameter
     *
     * @return int
     */
    public function getSize(): int
    {
        $size = $this->get('size', $this->defaultSize);

        return $size < self::MAX_SIZE ? $size : self::MAX_SIZE;
    }

    /**
     * Get "_source" parameter
     *
     * @param  array|bool  $fields
     *
     * @return $this|Request
     */
    public function source($fields = [])
    {
        if ($fields == false) {
            return $this->set('_source', false);
        }

        $fields = is_array($fields) ? $fields : func_get_args();

        return $this->set('_source', $fields);
    }

    /**
     * Get "_source" parameter
     *
     * @return bool|string|null
     */
    public function getSource()
    {
        return $this->get('_source') ?? false;
    }

    /**
     * Get "aggs-size" parameter
     *
     * @return int
     */
    public function getAggregationsSize(): int
    {
        $size = $this->get('aggs-size', $this->defaultAggregationsSize);

        return $size < self::MAX_SIZE ? $size : self::MAX_SIZE;
    }

    /**
     * Set aggregations logic
     *
     * @param  AggregationContract  $aggregation
     *
     * @return self
     */
    public function aggregation(AggregationContract $aggregation): self
    {
        $this->aggregations[$aggregation->name()] = $aggregation->toArray();

        return $this;
    }

    /**
     * Get aggregations logic
     *
     * @return array
     */
    public function getAggregations(): ?array
    {
        return $this->aggregations;
    }

    /**
     * @param  array|null  $sorts
     *
     * @return $this
     */
    public function setSort(?array $sorts): self
    {
        $this->sort = $sorts;

        return $this;
    }

    /**
     * Set sort logic
     *
     * @param  SortContract  $sort
     *
     * @return self
     */
    public function sort(SortContract $sort): self
    {
        $this->sort[] = $sort->toArray();

        return $this;
    }

    /**
     * Get "sort" logic
     *
     * @return array
     */
    public function getSort(): ?array
    {
        return $this->sort;
    }

    /**
     * Set highlight logic
     *
     * @param  array  $fields
     *
     * @return self
     */
    public function highlight($fields = []): self
    {
        $this->highlight = is_array($fields) ? $fields : func_get_args();

        return $this;
    }

    /**
     * Set highlight tags
     *
     * @param  string  $openTag
     * @param  string  $closeTag
     *
     * @return $this
     */
    public function highlightTags(string $openTag, string $closeTag): self
    {
        $this->highlightTags[0] = $openTag;
        $this->highlightTags[1] = $closeTag;

        return $this;
    }

    /**
     * Get highlight tags
     *
     * @return array
     */
    public function getHighlightTags(): array
    {
        return $this->highlightTags;
    }


    /**
     * Get highlight logic
     *
     * @return array
     */
    protected function getHighlight(): ?array
    {
        if (empty($this->highlight)) {
            return [];
        }

        $fields = array_map(function ($field) {
            return [$field => ['type' => 'plain']];
        }, $this->highlight);

        return [
            "pre_tags" => [$this->highlightTags[0]],
            "post_tags" => [$this->highlightTags[1]],
            "fields" => $fields
        ];
    }

    /**
     * Format empty data
     *
     * @param  array|null  $array
     *
     * @return array|stdClass|null
     */
    protected function allowEmpty(?array $array)
    {
        return empty($array) ? new stdClass() : $array;
    }

    /**
     * Create expression
     *
     * @param  ExpressionContract  $expression
     *
     * @return mixed
     */
    protected function expression(ExpressionContract $expression)
    {
        return $expression->toArray();
    }

    /**
     * Debug current request
     *
     * @param  array  $data
     *
     * @return void
     */
    public function debug(array $data)
    {
        echo '<h3 style="color: cadetblue">'.get_class($this).'</h3>';
        echo jsonBeautifier($data);
    }

    protected function debugExecutionTime(float $time)
    {
        $time = microtime(true) - $time;

        $color = "black";
        if ($time < 0.2) {
            $color = "green";
        } elseif ($time < 0.5) {
            $color = 'orange';
        } else {
            $color = 'red';
        }
        echo "<br>";
        echo "Execution time: <span style='color:$color'>$time</span>";
        echo "<hr>";
    }


    /**
     * Get response class name
     *
     * @return ResponseContract
     */
    public function response(): ResponseContract
    {
        return new SingleResponse();
    }

    /**
     * Index name
     *
     * @return string
     */
    abstract public function index(): string;

    /**
     * Declare validation rules
     *
     * @return array
     */
    abstract public function validationRules(): array;

    /**
     * Request query
     *
     * @return array
     */
    abstract public function query(): array;
}
