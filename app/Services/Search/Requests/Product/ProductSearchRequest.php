<?php


namespace App\Services\Search\Requests\Product;


use App\Services\Search\Contracts\ResponseContract;
use App\Services\Search\Entities\Product;
use App\Services\Search\Enums\SortOrder;
use App\Services\Search\Helpers\Params;
use App\Services\Search\Queries\Product\ProductSearchQuery;
use App\Services\Search\Requests\SingleRequest;
use App\Services\Search\Responses\Product\ProductResponse;
use App\Services\Search\Sorts\FieldSort;

class ProductSearchRequest extends SingleRequest
{
    /**
     * Get response class name
     *
     * @return ResponseContract
     */
    public function response(): ResponseContract
    {
        return new ProductResponse();
    }

    /**
     * Index name
     *
     * @return string
     */
    public function index(): string
    {
        return Product::_INDEX_;
    }

    /**
     * Declare validation rules
     *
     * @return array
     */
    public function validationRules(): array
    {
        return [
            'query' => 'required|string',
            'lang' => 'required|string|size:2',
            'page' => 'integer|min:1',
        ];
    }

    /**
     * Initialize method
     */
    public function init(): void
    {
        // Track total hits param
        $this->trackTotalHits(10000);

        /**
         * Source fields
         */
        $this->source(
            Product::NAME,
            Product::PRICE
        );

        // Add sort logic
        $this->sort(new FieldSort(Product::STATE_STOCK, SortOrder::DESC));

        // Set page
        $this->page($this->get('page', 1));

        // Setting size
        // we took size from http-query parameter as priority or default size for generics autocomplete from
        // config file, @see config/services
        $this->size($this->get('size', Params::size(Product::_TYPE_)));
    }

    /**
     * Request query
     *
     * @return array
     */
    public function query(): array
    {
        return $this->compileQuery(new ProductSearchQuery());
    }
}
