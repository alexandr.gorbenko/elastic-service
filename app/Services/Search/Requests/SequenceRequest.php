<?php


namespace App\Services\Search\Requests;


use App\Services\Search\Contracts\ResponseContract;
use App\Services\Search\Facades\Search;
use App\Services\Search\Request;
use App\Services\Search\RequestBag;
use App\Services\Search\Responses\SingleResponse;

abstract class SequenceRequest extends Request
{
    /**
     * @return ResponseContract
     */
    public function execute(): ResponseContract
    {
        $response = null;

        foreach ($this->requestBag()->getRequests() as $request) {
            $response = Search::request($request)->execute();
            if ($response->hasResults()) {
                return $response;
            }
        }

        return $response;
    }

    /**
     * @return ResponseContract
     */
    public function response(): ResponseContract
    {
        return new SingleResponse();
    }

    public function init(): void
    {
    }


    /**
     * @return RequestBag
     */
    abstract public function requestBag(): RequestBag;
}