<?php


namespace App\Services\Search\Requests;


use App\Services\Search\Contracts\ResponseContract;
use App\Services\Search\Facades\Search;
use App\Services\Search\Request;
use App\Services\Search\Response;
use App\Services\Search\Responses\MultiResponse;

abstract class MultiRequest extends Request
{

    public function execute(): ResponseContract
    {
        $response = $this->createResponse(Response::TYPE_SUCCESS);

        foreach ($this->requests() as $key => $request) {
            $response->add($key, $request->execute());
        }

        return $response;
    }

    protected function request(SingleRequest $request, array $parameters = []): SingleRequest
    {
        $parameters = array_replace($this->parameters, $parameters);

        return Search::singleRequest($request, $parameters);
    }

    /**
     * Get response class name
     *
     * @return ResponseContract
     */
    public function response(): ResponseContract
    {
        return new MultiResponse();
    }

    /**
     * Initialize all requests
     */
    abstract public function requests(): array;


}
