<?php


namespace App\Services\Search;


use App\Services\Search\Contracts\AggregationContract;

abstract class Aggregation implements AggregationContract
{
    /**
     * Max count of items
     */
    const MAX_SIZE = 10000;

    /**
     * @var string
     */
    protected $name;

    /**
     * Aggregation constructor.
     *
     * @param  string  $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Aggregation name
     *
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}