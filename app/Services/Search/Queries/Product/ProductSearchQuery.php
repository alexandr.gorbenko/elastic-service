<?php


namespace App\Services\Search\Queries\Product;


use App\Services\Search\Entities\Product;
use App\Services\Search\Queries\MultiMatchQuery;
use App\Services\Search\Query;


class ProductSearchQuery extends Query
{
    /**
     * Required parameters
     *
     * @return array
     */
    public function required(): array
    {
        return ['query', 'lang'];
    }

    /**
     * Query body
     *
     * @return array
     */
    protected function toArray(): array
    {
        return $this->createQuery(new MultiMatchQuery())
            ->fields(
                field(Product::NAME, 2),
                field(Product::DESCRIPTION)
            )
            ->compile();
    }

}
