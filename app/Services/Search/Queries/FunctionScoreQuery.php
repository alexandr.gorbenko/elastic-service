<?php


namespace App\Services\Search\Queries;


use App\Services\Search\Query;

class FunctionScoreQuery extends Query
{
    /**
     * Required parameters
     *
     * @return array
     */
    public function required(): array
    {
        return ['score'];
    }

    /**
     * Query body
     *
     * @return array
     */
    protected function toArray(): array
    {
        return [
            'function_score' => [
                'query' => $this->compileQuery(new MultiMatchQuery()),
                'script_score' => [
                    'script' => [
                        'params' => (object)$this->get('params', []),
                        'source' => $this->score()
                    ]
                ]
            ]
        ];
    }

    private function score(): string
    {
        $score = $this->get('score');

        if (is_array($score)) {
            return implode(' * ', $this->get('score'));
        }

        return $score;
    }

}
