<?php


namespace App\Services\Search\Queries;


use App\Services\Search\Enums\QueryTerm;
use App\Services\Search\Query;

class BestMatchQuery extends Query
{

    /**
     * Required parameters
     *
     * @return array
     */
    public function required(): array
    {
        return [];
    }

    /**
     * Query body
     *
     * @return array
     */
    protected function toArray(): array
    {
        if (empty($this->get('matches'))) {
            return $this->compileQuery(new MatchAllQuery());
        }

        return [
            "bool" => [
                'minimum_should_match' => $this->get('minimum_should_match', 0),
                QueryTerm::MUST => $this->get(QueryTerm::MUST, []),
                QueryTerm::SHOULD => $this->get('matches', []),
                QueryTerm::MUST_NOT => $this->get(QueryTerm::MUST_NOT, []),
                QueryTerm::FILTER => $this->get(QueryTerm::FILTER, [])
            ]
        ];
    }
}