<?php


namespace App\Services\Search\Queries;


use App\Services\Search\Query;

class TermsQuery extends Query
{
    /**
     * Required parameters
     *
     * @return array
     */
    public function required(): array
    {
        return ['terms'];
    }

    /**
     * Query body
     *
     * @return array
     */
    protected function toArray(): array
    {
        return [
            'bool' => [
                'filter' => [
                    "terms" => $this->get('terms')
                ]
            ]
        ];
    }

}
