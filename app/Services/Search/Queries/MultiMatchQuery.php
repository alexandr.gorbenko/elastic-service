<?php


namespace App\Services\Search\Queries;


use App\Services\Search\Enums\QueryTerm;
use App\Services\Search\Query;

class MultiMatchQuery extends Query
{
    /**
     * Required parameters
     *
     * @return array
     */
    public function required(): array
    {
        return ['query', 'fields'];
    }

    /**
     * Query body
     *
     * @return array
     */
    protected function toArray(): array
    {
        if (empty($this->get('query'))) {
            return $this->compileQuery(new MatchAllQuery());
        }

        return $this->createQuery(new BoolQuery())
            ->add(QueryTerm::SHOULD, [
                'multi_match' => $this->multiMatch()
            ])
            ->compile();
    }

    protected function multiMatch(): array
    {
        $params = [
            'query' => $this->get('query'),
            'fields' => $this->get('fields'),
            'type' => $this->get('match_type', 'best_fields'),
            'operator' => $this->get('operator', 'or')
        ];

        if (!in_array($this->get('match_type'), ['phrase', 'phrase_prefix'])) {
            $params['fuzziness'] = $this->get('fuzziness', 0);
        }

        return $params;
    }


}
