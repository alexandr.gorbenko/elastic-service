<?php


namespace App\Services\Search\Queries;


use App\Services\Search\Enums\QueryTerm;
use App\Services\Search\Query;

class BoolQuery extends Query
{
    /**
     * Required parameters
     *
     * @return array
     */
    public function required(): array
    {
        return [];
    }

    /**
     * Query body
     *
     * @return array
     */
    protected function toArray(): array
    {
        return [
            'bool' => array_filter([
                'minimum_should_match' => $this->get('minimum_should_match', 1),
                QueryTerm::MUST => $this->get(QueryTerm::MUST, []),
                QueryTerm::SHOULD => $this->get(QueryTerm::SHOULD, []),
                QueryTerm::MUST_NOT => $this->get(QueryTerm::MUST_NOT, []),
                QueryTerm::FILTER => $this->get(QueryTerm::FILTER, [])
            ])
        ];
    }
}
