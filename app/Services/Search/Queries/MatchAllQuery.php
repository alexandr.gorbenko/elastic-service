<?php


namespace App\Services\Search\Queries;


use App\Services\Search\Enums\QueryTerm;
use App\Services\Search\Query;

class MatchAllQuery extends Query
{
    /**
     * Required parameters
     *
     * @return array
     */
    public function required(): array
    {
        return [];
    }

    /**
     * Query body
     *
     * @return array
     */
    protected function toArray(): array
    {
        return $this->createQuery(new BoolQuery())
            ->replace(QueryTerm::SHOULD, [
                'match_all' => (object)[]
            ])
            ->compile();
    }

}
