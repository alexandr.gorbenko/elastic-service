<?php


namespace App\Services\Search\Queries;


use App\Services\Search\Query;

class TermQuery extends Query
{
    /**
     * Required parameters
     *
     * @return array
     */
    public function required(): array
    {
        return ['term'];
    }

    /**
     * Query body
     *
     * @return array
     */
    protected function toArray(): array
    {
        return [
            'bool' => [
                'filter' => [
                    "term" => $this->get('term')
                ]
            ]
        ];
    }

}
