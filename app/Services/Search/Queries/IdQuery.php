<?php


namespace App\Services\Search\Queries;


use App\Services\Search\Query;

class IdQuery extends Query
{
    /**
     * Required parameters
     *
     * @return array
     */
    public function required(): array
    {
        return ['type', 'ids'];
    }

    /**
     * Query body
     *
     * @return array
     */
    protected function toArray(): array
    {
        return [
            'ids' => [
                'type' => $this->get('type'),
                'values' => $this->get('ids')
            ]
        ];
    }

}
