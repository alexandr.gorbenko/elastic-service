<?php


namespace App\Services\Search;


use App\Services\Search\Contracts\AnalyzerContract;
use App\Services\Search\Contracts\AnalyzerRuleContract;
use App\Services\Search\Exceptions\AnalyzerRuleException;
use App\Services\Search\Responses\SingleResponse;

abstract class Analyzer implements AnalyzerContract
{
    /**
     * @var Query
     */
    protected $query;
    /**
     * @var SingleResponse
     */
    protected $response;
    /**
     * @var array
     */
    protected $items;
    /**
     * @var bool
     */
    protected $modifyQueryString;
    /**
     * @var array
     */
    protected $rules;

    /**
     * Analyzer constructor.
     *
     * @param  Query  $query
     * @param  SingleResponse  $response
     * @param  bool  $modifyQueryString
     */
    public function __construct(Query $query, SingleResponse $response)
    {
        $this->query = $query;

        $this->response = $response;
        $this->items = $response->items();

        $this->analyze();
    }

    /**
     * @param  AnalyzerRuleContract  $rule
     *
     * @return mixed|void
     */
    public function addRule(AnalyzerRuleContract $rule)
    {
        $this->rules[] = $rule;
        return $this;
    }

    /**
     * @return Query
     */
    public function getQuery(): Query
    {
        return $this->query;
    }

    /**
     * @param  array  $items
     */
    public function setItems(array $items)
    {
        $this->items = array_values($items);
    }

    /**
     * @return array
     */
    public function items(): array
    {
        return $this->items;
    }

    /**
     * @return bool
     */
    public function succeed(): bool
    {
        return !empty($this->items());
    }

    /**
     * @return bool
     */
    public function fails(): bool
    {
        return !$this->succeed();
    }

    /**
     * @return array
     */
    public function ids(): array
    {
        return array_map(function ($item) {
            return $item[Mapping::FIELD_ID];
        }, $this->items());
    }

    public function canAnalyze()
    {
        return !empty($this->items) && !empty($this->query->get('query'));
    }

    /**
     * Analyze items
     */
    public function analyze()
    {
        foreach ($this->rules() as $rule) {
            if(!($rule instanceof AnalyzerRuleContract)) {
                throw new AnalyzerRuleException('Analyzer rule must be instance of AnalyzerRuleContract');
            }

            if(!$this->canAnalyze()) {
                break;
            }

            $rule->setAnalyzer($this);
            $rule->execute();
        }
    }


    /**
     * Rules
     */
    abstract protected function rules();


}