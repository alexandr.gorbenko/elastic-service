<?php


namespace App\Services\Search;


class Entity
{
    /**
     * Entity index
     */
    const _INDEX_ = 'default';
    /**
     * Entity type
     */
    const _TYPE_ = 'default';

    /**
     * Entity ID field
     */
    const ID = 'id';

    /**
     * @return string
     */
    public function index(): string
    {
        return static::_INDEX_;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return static::_TYPE_;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return static::ID;
    }
}