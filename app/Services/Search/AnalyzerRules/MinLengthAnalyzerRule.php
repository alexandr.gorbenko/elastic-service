<?php


namespace App\Services\Search\AnalyzerRules;


use App\Services\Search\AnalyzerRule;

class MinLengthAnalyzerRule extends AnalyzerRule
{
    /**
     * @var int
     */
    protected $minLength;

    /**
     * MinLengthAnalyzerRule constructor.
     *
     * @param $minLength
     */
    public function __construct(int $minLength = 3)
    {
        $this->minLength = $minLength;
    }

    /**
     * Execute rule
     */
    public function execute()
    {
        if (strlen($this->analyzer->getQuery()->get('query')) < $this->minLength) {
            $this->analyzer->setItems([]);
        }
    }
}