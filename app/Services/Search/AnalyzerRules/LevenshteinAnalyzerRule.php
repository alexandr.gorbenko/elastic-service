<?php


namespace App\Services\Search\AnalyzerRules;


use App\Services\Search\AnalyzerRule;
use App\Services\Search\Mapping;

class LevenshteinAnalyzerRule extends AnalyzerRule
{
    /**
     * @var int
     */
    protected $maxDistance;
    /**
     * @var bool
     */
    protected $modifyQueryString;

    /**
     * LevenshteinAnalyzerRule constructor.
     *
     * @param  int  $maxDistance
     * @param  bool  $modifyQueryString
     */
    public function __construct(int $maxDistance, bool $modifyQueryString = false)
    {
        $this->maxDistance = $maxDistance;
        $this->modifyQueryString = $modifyQueryString;
    }

    /**
     * Execute rule
     */
    public function execute()
    {
        $sourceItems = array_combine(
            array_column($this->analyzer->items(), Mapping::FIELD_ID),
            array_column($this->analyzer->items(), Mapping::FIELD_SOURCE)
        );

        $queryString = explode(' ', $this->analyzer->getQuery()->get('query'));
        $words = array_map('mb_strtolower', $queryString);
        $words = array_map('ascii', $words);

        $hits = 0;
        $exclude = [];

        foreach ($words as $index => $word) {

            if(preg_replace('/[^\pL]/u', '', $word) != $word) {
                continue;
            }

            $items = $sourceItems;

            foreach ($items as $id => &$source) {
                $values = [];
                array_walk_recursive($source, function ($value) use (&$values, $word) {
                    $value = ascii(mb_strtolower($value));

                    $valueWords = getWords($value, true);

                    if(count($valueWords) > 1) {
                        $wordValues = [];

                        foreach ($valueWords as $valueWord) {
                            $wordValues[] = levenshtein($word, $valueWord);
                        }

                        $values[] = max($wordValues) > 1 ? min($wordValues) + 1 : min($wordValues);

                    } else {
                        $values[] = levenshtein($word, $value);
                    }


                });
                $source = min($values);
            }

            $filtered = array_keys(array_filter($items, function ($distance) {
                return $distance <= $this->maxDistance;
            }));

            if (!empty($filtered)) {
                $hits += count($filtered);

                if ($this->modifyQueryString) {
                    $exclude[] = $index;
                }

                $this->analyzer->setItems(array_filter($this->analyzer->items(), function ($item) use ($filtered) {
                    return in_array($item[Mapping::FIELD_ID], $filtered);
                }));
            }
        }

        $queryString = array_filter($queryString, function ($index) use ($exclude) {
            return !in_array($index, $exclude);
        }, ARRAY_FILTER_USE_KEY);

        $this->analyzer->getQuery()->query(implode(' ', $queryString));

        if ($hits == 0) {
            $this->analyzer->setItems([]);
        }
    }


}