<?php


namespace App\Services\Search\Exceptions;


use App\Services\Search\Exception;

class ResponseTypeException extends Exception
{
    function code(): int
    {
        return 603;
    }

}