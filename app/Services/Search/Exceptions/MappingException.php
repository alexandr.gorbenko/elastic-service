<?php


namespace App\Services\Search\Exceptions;


use App\Services\Search\Exception;

class MappingException extends Exception
{
    function code(): int
    {
        return 601;
    }


}