<?php


namespace App\Services\Search\Exceptions;


use App\Services\Search\Exception;

class QueryParametersException extends Exception
{
    function code(): int
    {
        return 602;
    }

}
