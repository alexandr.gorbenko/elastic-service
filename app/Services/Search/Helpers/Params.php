<?php


namespace App\Services\Search\Helpers;


use App\Services\Search\Entities\Product;

class Params
{
    const SIZE = 'size';
    const AUTOCOMPLETE_SIZE = 'autocomplete-size';

    const WEIGHT_MIN = 'weight-min';
    const WEIGHT_MAX = 'weight-max';

    const SCORE_MIN = 'score-min';
    const SCORE_MAX = 'score-max';

    const PRICE_MIN = 'price-min';
    const PRICE_MAX = 'price-max';

    const HIGHLIGHT_TAG_OPEN = 'highlight_tag_open';
    const HIGHLIGHT_TAG_CLOSE = 'highlight_tag_close';

    /**
     * @param  string  $type
     *
     * @return int
     */
    public static function size(string $type): int
    {
        return self::params($type, self::SIZE);
    }

    /**
     * @param  string  $type
     *
     * @return int
     */
    public static function autocompleteSize(string $type): int
    {
        return self::params($type, self::AUTOCOMPLETE_SIZE);
    }

    /**
     * @param  string  $type
     *
     * @return float
     */
    public static function minWeight(string $type): float
    {
        return self::params($type, self::WEIGHT_MIN);
    }

    /**
     * @param  string  $type
     *
     * @return float
     */
    public static function maxWeight(string $type): float
    {
        return self::params($type, self::WEIGHT_MAX);
    }

    /**
     * @param  string  $type
     *
     * @return float
     */
    public static function minScore(string $type): float
    {
        return self::params($type, self::SCORE_MIN);
    }

    /**
     * @param  string  $type
     *
     * @return float
     */
    public static function maxScore(string $type): float
    {
        return self::params($type, self::SCORE_MAX);
    }

    /**
     * @return float
     */
    public static function minPrice(): float
    {
        return self::params(Product::_TYPE_, self::PRICE_MIN);
    }

    /**
     * @return float
     */
    public static function maxPrice(): float
    {
        return self::params(Product::_TYPE_, self::PRICE_MAX);
    }

    /**
     * @param  string  $type
     *
     * @return string
     */
    public static function highlightTagOpen(string $type): string
    {
        return self::params($type, self::HIGHLIGHT_TAG_OPEN);
    }

    /**
     * @param  string  $type
     *
     * @return string
     */
    public static function highlightTagClose(string $type): string
    {
        return self::params($type, self::HIGHLIGHT_TAG_CLOSE);
    }

    /**
     * @param  string  $type
     * @param  string  $param
     *
     * @return mixed
     */
    private static function params(string $type, string $param)
    {
        static $params;

        if (!$params) {
            $default = config("search.params._default");

            foreach (config("search.params") as $key => $value) {
                if ($key == '_default') {
                    continue;
                }

                $params[$key] = array_replace_recursive(
                    $default,
                    $value,
                    config(field('search.clients', app()->currentClient, 'params', $key), [])
                );
            }
        }

        return $params[$type][$param] ?? null;
    }

}
