<?php


namespace App\Services\Search\Helpers;


use Illuminate\Database\Query\Expression as QueryExpression;

class Alias
{
    /**
     * @var string
     */
    protected $field;
    /**
     * @var string
     */
    protected $alias;

    /**
     * Alias constructor.
     *
     * @param  string|QueryExpression  $field
     * @param  string  $alias
     */
    public function __construct($field, string $alias)
    {
        $this->setField($field);
        $this->setAlias($alias);
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param  string  $field
     */
    public function setField($field): void
    {
        $this->field = $field;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @param  string  $alias
     */
    public function setAlias(string $alias): void
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function compile()
    {
        if ($this->field instanceof QueryExpression) {
            return new QueryExpression(new Alias((string)$this->field, '`'.$this->alias.'`'));
        }

        return $this->field.' as '.$this->alias;
    }

    /**
     * @param  string  $name
     * @param  array  $aliases
     *
     * @return QueryExpression
     */
    public static function group(string $name, array $aliases)
    {
        foreach ($aliases as &$alias) {
            $alias = static::parse($alias);

            if (!($alias->getField() instanceof QueryExpression)) {
                $field = $alias->getField();
                $field = '`'.str_replace('.', '`.`', $field).'`';
                $alias->setField($field);
            }
            $alias->setAlias('`'.field($name, $alias->getAlias()).'`');
        }

        return new QueryExpression(implode(', ', $aliases));
    }

    /**
     * @param  string  $alias
     *
     * @return static
     */
    public static function parse($alias): self
    {
        if ($alias instanceof QueryExpression) {
            $alias = $alias->getValue();

            return new Alias(new QueryExpression($alias->getField()), $alias->getAlias());
        }

        list($field, $value) = explode(' as ', $alias);

        return new Alias($field, $value);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->compile();
    }

}