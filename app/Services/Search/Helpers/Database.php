<?php


namespace App\Services\Search\Helpers;


class Database
{
    /**
     * @param  string  $table
     * @param  string|null  $alias
     *
     * @return string
     */
    public static function getFrontendTable(string $table, string $alias = null): string
    {
        return self::generateTableString('pkw_frontend', $table, $alias);
    }

    /**
     * @param  string  $table
     * @param  string|null  $alias
     *
     * @return string
     */
    public static function getPkwTable(string $table, string $alias = null): string
    {
        return self::generateTableString('pkwteile_de', $table, $alias);
    }

    /**
     * @param  string  $table
     * @param  string|null  $alias
     *
     * @return string
     */
    public static function getStoreTable(string $table, string $alias = null): string
    {
        return self::generateTableString('pkwteile_store', $table, $alias);
    }

    /**
     * @param  string  $table
     * @param  string|null  $alias
     *
     * @return string
     */
    public static function getTecdocTable(string $table, string $alias = null): string
    {
        return self::generateTableString('tecdoc_new', $table, $alias);
    }

    /**
     * @param  string  $database
     * @param  string  $table
     * @param  string|null  $alias
     *
     * @return string
     */
    public static function generateTableString(string $database, string $table, string $alias = null): string
    {
        $string = $database.'.'.$table;

        if (!empty($alias)) {
            $string = alias($string, $alias);
        }

        return $string;
    }

}