<?php


namespace App\Services\Search\Helpers;


use App\Services\Search\Contracts\RequestContract;
use Closure;
use Illuminate\Support\Facades\Cache;

class Memory
{
    /**
     * @var array
     */
    protected static $data;

    /**
     * @param  string  $key
     *
     * @return bool
     */
    public static function has(string $key): bool
    {
        if (isset(self::$data[$key])) {
            return true;
        }

        if (self::enableCache()) {
            return Cache::has($key);
        }

        return false;
    }

    /**
     * @param  string  $key
     * @param  array  $value
     */
    public static function set(string $key, array $value): void
    {
        self::$data[$key] = $value;

        if (self::enableCache()) {
            Cache::put($key, $value, 60 * 10);
        }
    }

    /**
     * @param  string  $key
     *
     * @return array|null
     */
    public static function get(string $key): ?array
    {
        if ($data = self::$data[$key]) {
            return $data;
        }

        if (self::enableCache()) {
            return Cache::get($key, null);
        }

        return null;
    }

    /**
     * @param  string  $key
     * @param  Closure  $callback
     *
     * @return array|null
     */
    public static function remember(string $key, Closure $callback): ?array
    {
        if (self::has($key)) {
            return self::get($key);
        }

        self::set($key, $result = $callback());

        return $result;
    }

    public static function clear(): void
    {
        self::$data = [];

        if (self::enableCache()) {
            Cache::store()->clear();
        }
    }

    /**
     * @param  RequestContract  $request
     * @param  array  $parameters
     *
     * @return string
     */
    public static function key(RequestContract $request, array $parameters): string
    {
        return sha1(get_class($request).json_encode($parameters));
    }

    /**
     * @return bool
     */
    private static function enableCache(): bool
    {
        return config('cache.enable', false);
    }

}
