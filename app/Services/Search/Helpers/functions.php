<?php

use App\Services\Search\Contracts\ArrayableContract;
use App\Services\Search\Contracts\ExecutableContract;
use App\Services\Search\Enums\SearchProperty;
use App\Services\Search\Facades\Search;
use App\Services\Search\Helpers\Alias;
use App\Services\Search\Helpers\Database;
use App\Services\Search\Mapping;
use Illuminate\Support\Arr;

/**
 * Returns current index name with version prefix
 */
if (!function_exists('getIndexName')) {
    function getIndexName(string $index): string
    {
        $versions = explode('.', Search::version());
        $majorVersion = !empty($versions[0]) ? (int)$versions[0] : 0;
        $index = strtolower($index);

        return "v{$majorVersion}-{$index}";
    }
}

/**
 * Create field name string from parts
 */
if (!function_exists('field')) {
    function field($parts = []): string
    {
        $parts = is_array($parts) ? $parts : func_get_args();

        $boost = '';

        if (is_int(last($parts))) {
            $boost = '^'.array_pop($parts);
        }

        return implode('.', $parts).$boost;
    }
}

/**
 * Create autocomplete field name string from parts
 */
if (!function_exists('autocomplete')) {
    function autocomplete($parts = []): string
    {
        $parts = is_array($parts) ? $parts : func_get_args();

        $boost = null;

        if (is_int(last($parts))) {
            $boost = array_pop($parts);
        }

        array_push($parts, Mapping::FIELD_AUTOCOMPLETE);

        if (isset($boost)) {
            array_push($parts, $boost);
        }

        return field($parts);
    }
}

/**
 * Query string
 */
if (!function_exists('queryString')) {
    function queryString(string $string = null): string
    {
        static $queryString;

        if (is_null($queryString)) {
            $queryString = mb_strtolower(trim(app('request')->get('query')));
        }

        if (!is_null($string)) {
            $queryString = mb_strtolower(trim($string));
        }

        return $queryString;
    }
}

/**
 * Get different words combinations as array of strings
 */
if (!function_exists('wordsCombinations')) {
    function wordsCombinations(array $words = []): array
    {
        if (empty($words)) {
            return [];
        }

        $combinations = [];

        for ($i = 0; $i < count($words); $i++) {
            $prefix = isset($combinations[$i - 1]) ? $combinations[$i - 1].' ' : '';
            $combinations[] = $prefix.$words[$i];
        }

        while (!empty($words)) {
            array_shift($words);
            $combinations = array_merge($combinations, wordsCombinations($words));
        }

        return array_values(array_unique($combinations));
    }
}

/**
 * Convert UTF-8 to ASCII
 */
if (!function_exists('ascii')) {
    function ascii(string $text): string
    {
        static $patterns;
        static $replacements;

        if (empty($patterns) || empty($replacements)) {
            $ascii = config('ascii');

            $patterns = array_keys($ascii);
            $replacements = array_values($ascii);
        }

        return preg_replace($patterns, $replacements, $text);
    }
}

/**
 * Return SearchProperty field
 */
if (!function_exists('searchProperty')) {
    function searchProperty(string $name): string
    {
        return SearchProperty::getField($name);
    }
}

/**
 * Scale value from one min/max rules to another
 */
if (!function_exists('transformValue')) {
    function transformValue(
        float $value,
        float $fromMin,
        float $fromMax,
        float $toMin,
        float $toMax,
        int $precision = 2
    ): float {
        if ($fromMin == $fromMax || $toMin == $toMax) {
            return 1;
        }

        if ($value < $fromMin) {
            return $toMin;
        }

        if ($value > $fromMax) {
            return $toMax;
        }

        return round(($value - $fromMin) / ($fromMax - $fromMin) * ($toMax - $toMin) + $toMin, $precision);
    }
}

/**
 * Get languages names array or language name by id
 */
if (!function_exists('languages')) {
    function languages(int $id = null)
    {
        static $languages;

        if (!isset($languages)) {
            $languages = config('languages', []);
        }

        if (isset($id)) {
            return $languages[$id] ?? null;
        }

        return $languages;
    }
}

/**
 * Json beautifier
 */
if (!function_exists('jsonBeautifier')) {
    function jsonBeautifier(array $array): string
    {
        $json = json_encode($array, JSON_PRETTY_PRINT);

        if (json_last_error() != JSON_ERROR_NONE) {
            die(json_last_error_msg());
        }

        $json = str_replace(['<', '>'], ['&#60;', '&#62;'], $json);
        $json = preg_replace('/([\\p{Ps}\\p{Pe}])/', '<span style=\'color:grey\'>$1</span>', $json);
        $json = preg_replace('/\"([\p{L}\p{N}\S]+)\"\s*\:/', '"<span style=\'color:blue\'>$1</span>":', $json);
        $json = preg_replace('/\"([\p{L}\p{N}\S]+.*)\"/', '"<span style=\'color:green\'>$1</span>"', $json);
        $json = preg_replace('/([0-9\.]+)([\,\\n]+)/', '<span style=\'color:red\'>$1</span>$2', $json);
        $json = preg_replace('/("(\s*):)/', '"<span style=\'color:grey\'>:</span>', $json);
        $json = preg_replace('/([",]{1})/', '<span style=\'color:grey\'>$1</span>', $json);
        $json = preg_replace('/(true|false)/', '<span style=\'color:purple\'>$1</span>', $json);

        return '<pre style="margin: 0">'.$json.'</pre>';
    }
}

/**
 * Transform multi-dimension array to one-dimension with "." (dot)
 */
if (!function_exists('arrayToDots')) {
    function arrayToDots(array $array, string $prepend = ''): array
    {
        $results = [];

        foreach ($array as $key => $value) {
            if (is_array($value) && !empty($value)) {
                $results = array_merge($results, arrayToDots($value, $prepend.$key.'.'));
            } else {
                if (is_numeric($key)) {
                    $results[substr($prepend, 0, -1)][] = $value;
                } else {
                    $results[$prepend.$key] = $value;
                }
            }
        }

        return $results;
    }
}

/**
 * Transform one-dimension key-dotted array to multi-dimension
 */
if (!function_exists('dotsToArray')) {
    function dotsToArray(array $array, array $map = null): array
    {
        $result = [];

        if (!empty($map)) {
            array_walk_recursive($map, function (&$value) use ($array) {
                $value = $array[$value];
            });

            return $map;
        }

        foreach ($array as $key => $value) {
            if (strpos($key, '.') !== false) {
                Arr::set($result, $key, $value);
            } else {
                $result[$key] = $value;
            }
        }

        return $result;
    }
}

/**
 * Lightweight group array by field
 */
if (!function_exists('groupBy')) {
    function groupBy(array $data, string $field): array
    {
        $result = [];

        foreach ($data as $datum) {
            if (is_object($datum)) {
                $result[$datum->{$field}][] = $datum;
            } elseif (is_array($datum)) {
                $result[$datum[$field]][] = $datum;
            }
        }

        return $result;
    }
}

/**
 * Check if Debug Mode is on
 */
if (!function_exists('isDebugMode')) {
    function isDebugMode(): bool
    {
        return config('app.debug', false);
    }
}

/**
 * Return value
 */
if (!function_exists('getValue')) {
    function getValue($value)
    {
        if (empty($value)) {
            return $value;
        }

        if (is_array($value)) {
            return array_map(function ($v) {
                return getValue($v);
            }, $value);
        }

        if ($value instanceof Closure) {
            return getValue($value());
        }

        if ($value instanceof ExecutableContract) {
            return getValue($value->execute());
        }

        if ($value instanceof ArrayableContract) {
            return getValue($value->toArray());
        }

        return $value;
    }
}

/**
 * Calculate words count in string
 */
if (!function_exists('getWords')) {
    function getWords(string $text, $unique = false)
    {
        $text = preg_replace('/([^\pL\d]+)/', ' ', $text);

        $words = explode(' ', $text);

        if ($unique) {
            $words = array_values(array_unique($words));
        }

        return $words;
    }
}

/**
 * Generate sql select alias
 */
if (!function_exists('alias')) {
    function alias($field, string $alias)
    {
        return (new Alias($field, $alias))->compile();
    }
}

/**
 * Generate sql select alias
 */
if (!function_exists('aliasGroup')) {
    function aliasGroup(string $group, array $aliases)
    {
        return Alias::group($group, $aliases);
    }
}

/**
 * Generate table path for "frontend" database
 */
if (!function_exists('dbFrontend')) {
    function dbFrontend(string $table, $alias = null)
    {
        return Database::getFrontendTable($table, $alias);
    }
}

/**
 * Generate table path for "pkw" database
 */
if (!function_exists('dbPkw')) {
    function dbPkw(string $table, $alias = null)
    {
        return Database::getPkwTable($table, $alias);
    }
}

/**
 * Generate table path for "store" database
 */
if (!function_exists('dbStore')) {
    function dbStore(string $table, string $alias = null)
    {
        return Database::getStoreTable($table, $alias);
    }
}

/**
 * Generate table path for "tecdoc" database
 */
if (!function_exists('dbTecdoc')) {
    function dbTecdoc(string $table, string $alias = null)
    {
        return Database::getTecdocTable($table, $alias);
    }
}
