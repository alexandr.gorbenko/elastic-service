<?php


namespace App\Services\Search;


use App\Services\Search\Contracts\ExpressionContract;
use App\Services\Search\Contracts\SortContract;

class Sort implements SortContract
{
    /**
     * @var ExpressionContract
     */
    protected $expression;

    /**
     * Sort constructor.
     *
     * @param  ExpressionContract  $expression
     */
    public function __construct(ExpressionContract $expression)
    {
        $this->expression = $expression;
    }

    /**
     * @return ExpressionContract
     */
    public function getExpression(): ExpressionContract
    {
        return $this->expression;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->getExpression()->toArray();
    }
}
