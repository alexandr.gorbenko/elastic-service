<?php


namespace App\Services\Search\DataProviders;


use App\Services\Search\DataProvider;
use App\Services\Search\Entities\Brand;
use App\Services\Search\Entities\Category;
use App\Services\Search\Entities\Product;
use App\Services\Search\Entity;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ProductDataProvider extends DataProvider
{
    public $bulkSize = 1000;


    protected function initData(): void
    {
    }

    protected function bulkData(): void
    {
    }

    /**
     * @param  array  $item
     *
     * @return array
     */
    protected function item(array $item): array
    {
        $item[Product::STATE_STOCK] = (bool)$item[Product::STATE_STOCK];

        return $item;
    }

    /**
     * @return Builder
     */
    public function mainQuery(): Builder
    {
        return DB::table('products')
            ->select([
                alias('id', Product::ID),
                alias('category_id', Product::CATEGORY),
                alias('brand_id', Product::BRAND),
                alias('name', Product::NAME),
                alias('description', Product::DESCRIPTION),
                alias('price', Product::PRICE),
                alias('stock', Product::STATE_STOCK),
            ]);
    }

    /**
     * @return Entity
     */
    public function entity(): Entity
    {
        return new Product();
    }
}
