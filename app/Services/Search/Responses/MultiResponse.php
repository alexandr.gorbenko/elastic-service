<?php


namespace App\Services\Search\Responses;


use App\Services\Search\Contracts\ResponseContract;
use App\Services\Search\Response;

class MultiResponse extends Response
{
    /**
     * @var array
     */
    protected $responses;

    public function hasResults(): bool
    {
        return !empty($this->getResponses());
    }


    /**
     * @param  string  $name
     * @param  ResponseContract  $response
     *
     * @return MultiResponse
     */
    public function add(string $name, ResponseContract $response): self
    {
        $this->responses[$name] = $response;

        return $this;
    }

    /**
     * Get responses array
     *
     * @return array
     */
    public function getResponses(): array
    {
        return $this->responses;
    }

    protected function data(): array
    {
        $data = [];
        foreach ($this->getResponses() as $name => $response) {
            $data[$name] = $response->items();
        }

        return $data;
    }


}
