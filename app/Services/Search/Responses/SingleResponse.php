<?php


namespace App\Services\Search\Responses;


use App\Services\Search\Mapping;
use App\Services\Search\Paginator;
use App\Services\Search\Response;

class SingleResponse extends Response
{
    /**
     * Total relation "equals"
     * Means total value equals real count of founded documents
     */
    const TOTAL_EQUALS = 'eq';
    /**
     * Total relation "greater then equals"
     * Means total value less then count of founded documents
     */
    const TOTAL_GREATER_THEN_EQUALS = 'gte';

    /**
     * @var array
     */
    private $aggregations;
    /**
     * @var bool
     */
    private $paginationMetadata = true;

    /**
     * Check if response has items
     *
     * @return bool
     */
    public function hasResults(): bool
    {
        return $this->total() > 0;
    }

    /**
     * Check if response is empty
     *
     * @return bool
     */
    public function empty(): bool
    {
        return !$this->hasResults();
    }

    /**
     * Get count of founded items
     *
     * @return int
     */
    public function total(): int
    {
        return $this->get('hits.total.value', 0);
    }

    /**
     * Get total relation
     *
     * @return string
     */
    public function totalRelation(): string
    {
        return $this->get('hits.total.relation', self::TOTAL_EQUALS);
    }

    /**
     * Get maximum score value for current search
     *
     * @return float
     */
    public function maxScore(): float
    {
        return $this->get('hits.max_score', 0);
    }

    /**
     * Get search hits
     *
     * @return array
     */
    public function hits(): array
    {
        return $this->get('hits.hits', []);
    }

    /**
     * Get items
     *
     * @return array
     */
    public function items(): array
    {
        $aliases = $this->aliases();

        return array_map(function ($item) use ($aliases) {
            $doc[Mapping::FIELD_ID] = (int)$item[Mapping::FIELD_ID];
//            $doc[Mapping::FIELD_SCORE] = $item[Mapping::FIELD_SCORE];

            if (!empty($item[Mapping::FIELD_HIGHLIGHT])) {
                $doc['_'.Mapping::FIELD_HIGHLIGHT] = $this->getHighlightArray($item);
            }

            if (empty($item[Mapping::FIELD_SOURCE])) {
                return $doc;
            }

            foreach (arrayToDots($item[Mapping::FIELD_SOURCE]) as $field => $value) {
                if (!empty($aliases[$field])) {
                    $field = $aliases[$field];
                }

                $doc[Mapping::FIELD_SOURCE][$field] = $value;
            }

            $doc[Mapping::FIELD_SOURCE] = dotsToArray($doc[Mapping::FIELD_SOURCE]);

            return $doc;
        }, $this->hits());
    }

    /**
     * Get items ids
     *
     * @return array
     */
    public function ids(): array
    {
        return array_map(function ($item) {
            return $item[Mapping::FIELD_ID];
        }, $this->hits());
    }

    /**
     * @param  string  $field
     *
     * @param  string|null  $key
     *
     * @return array
     */
    public function pluck(string $field, string $key = null): array
    {
        if (!empty($key)) {
            $data = array_map(function ($item) use ($field, $key) {
                return [
                    'key' => $item[$key] ?? $item[Mapping::FIELD_SOURCE][$key],
                    'value' => $item[$field] ?? $item[Mapping::FIELD_SOURCE][$field]
                ];
            }, $this->items());

            $result = [];
            foreach ($data as $datum) {
                $result[$datum['key']] = $datum['value'];
            }

            return $result;
        }

        return array_map(function ($item) use ($field) {
            return $item[Mapping::FIELD_SOURCE][$field];
        }, $this->items());
    }

    /**
     * Get aggregations data
     *
     * @param  null  $key
     * @param  null  $default
     *
     * @return array
     */
    public function aggregations($key = null, $default = null)
    {
        if (!isset($this->aggregations)) {
            $this->aggregations = $this->get('aggregations', []);

            if (empty($this->aggregations)) {
                return [];
            }

            $this->aggregations = array_map(function ($aggregation) {
                if (isset($aggregation['value'])) {
                    return $aggregation['value'];
                }

                if (!isset($aggregation['buckets'])) {
                    return $aggregation;
                }

                $data = array_combine(
                    array_column($aggregation['buckets'], 'key'),
                    array_column($aggregation['buckets'], 'doc_count')
                );

                return $data;
            }, $this->aggregations);
        }

        if (isset($key)) {
            return data_get($this->aggregations, $key, $default);
        }

        return $this->aggregations;
    }

    public function withoutPaginator(): self
    {
        $this->paginationMetadata = false;

        return $this;
    }


    /**
     * Get result data using "dot" notation
     *
     * @param  string  $path
     * @param  null  $default
     *
     * @return mixed
     */
    protected function get(string $path, $default = null)
    {
        return data_get($this->request->getResult(), $path, $default);
    }

    /**
     * Get response data
     *
     * @return array
     */
    protected function data(): array
    {
        $data = [];

        $items = $this->items();

        if ($this->request->getSize() > 0) {
            $paginator = new Paginator(
                $items,
                $this->total(),
                $this->totalRelation(),
                $this->request->getSize(),
                $this->request->getPage()
            );

            $data['items'] = $paginator->getItems();

            if ($this->paginationMetadata) {
                $data['paginator'] = $paginator->getMetadata();
            }
        }

        if ($this->get('aggregations')) {
            $data['aggregations'] = $this->aggregations();
        }

        return $data;
    }

    protected function getHighlightArray(array $item): array
    {
        $highlights = $item[Mapping::FIELD_HIGHLIGHT];

        $data = [];
        foreach ($highlights as $field => $highlight) {
            if (strpos($field, Mapping::FIELD_AUTOCOMPLETE) !== false) {
                $field = str_replace('.'.Mapping::FIELD_AUTOCOMPLETE, '', $field);
            }
            $data[$field] = reset($highlight);
        }

        return dotsToArray($data);
    }

    /**
     * @return array
     */
    public function aliases(): array
    {
        return [];
    }

}
