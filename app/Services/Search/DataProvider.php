<?php


namespace App\Services\Search;


use App\Services\Search\Contracts\DataProviderContract;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

abstract class DataProvider implements DataProviderContract
{
    /**
     * Flag for own delete row in tecdoc
     */
    const OWN_DATA_DELETED = 5;

    /**
     * How much documents will be got per one iteration
     *
     * @var int
     */
    public $bulkSize = 1000;

    /**
     * Offset value for bulk selection
     *
     * @var int
     */
    private $bulkOffset = 0;

    /**
     * Array of item ids for bulk selection
     *
     * @var array
     */
    private $bulkIds = [];

    /**
     * Items
     *
     * @var array
     */
    private $items;

    /**
     * @var Entity
     */
    private $entity;
    /**
     * @var int
     */
    private $thread;
    /**
     * @var int
     */
    private $threadCount;
    /**
     * @var array
     */
    private $filteredIds;

    /**
     * @param  int  $thread
     * @param  int  $threadCount
     */
    public function setThreadParameters(int $thread = 1, int $threadCount = 1): void
    {
        if ($thread < 1 || $thread > $threadCount) {
            throw new \Exception('Thread index must be >= 1 and <= '.$threadCount);
        }
        $this->thread = $thread;
        $this->threadCount = $threadCount;
    }

    /**
     * Init method
     */
    public function init(): void
    {
        $this->entity = $this->entity();
        $this->initItems();
        $this->initData();
    }

    /**
     * @return array
     */
    public function currentItems(): array
    {
        $items = $this->scrollItems();

        if (empty($items)) {
            return [];
        }

        $this->bulkData();

        foreach ($items as &$item) {
            $item = $this->item((array)$item);
        }

        return $items;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function total(): int
    {
        return count($this->items);
    }

    /**
     * @return Entity
     */
    public function getEntity(): Entity
    {
        return $this->entity;
    }

    /**
     * Set limit by ids
     *
     * @param  array  $ids
     */
    public function filterIds(array $ids): void
    {
        $this->filteredIds = $ids;
    }

    /**
     * @return array
     */
    protected function getBulkIds(): array
    {
        return $this->bulkIds;
    }

    /**
     * @param  string  $idField
     *
     * @return \Illuminate\Database\Query\Expression
     */
    protected function getBulkCondition(string $idField)
    {
        if (empty($this->getBulkIds())) {
            return DB::raw('1=0');
        }
        $min = min($this->getBulkIds());
        $max = max($this->getBulkIds());

        $conditions[] = $idField.' >= '.$min;
        $conditions[] = $idField.' <= '.$max;

        if ($this->threadCount > 1) {
            $conditions[] = $idField.' % '.$this->threadCount.' = '. ($this->thread - 1);

            $possibleIds = [];
            for ($i = $min; $i <= $max; $i += $this->threadCount) {
                $possibleIds[] = $i;
            }
            $excludeIds = array_diff($possibleIds, $this->getBulkIds());
            if(!empty($excludeIds)) {
                $conditions[] = $idField .' not in (' . implode(',',$excludeIds) .')';
            }

        }

        return DB::raw('('.implode(' and ', $conditions).')');
    }

    /**
     * @return array
     */
    private function scrollItems(): array
    {
        if ($this->bulkOffset > count($this->items)) {
            return [];
        }

        $items = array_slice($this->items, $this->bulkOffset, $this->bulkSize);

        $this->bulkIds = array_column($items, $this->entity->id());

        $this->bulkOffset += $this->bulkSize;

        return $items;
    }

    /**
     * Get items from database
     */
    private function initItems(): void
    {
        // remove only full group by
        DB::statement("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));");

        $query = $this->mainQuery();

        if ($this->multiThreading()) {
            $query->whereRaw($this->idField().' % '.$this->threadCount.' = '.($this->thread - 1));
        }

        if (!empty($this->filteredIds)) {
            $query = $query->whereIn($this->idField(), $this->filteredIds);
        }

        foreach ($query->cursor() as $item) {
            $this->items[] = $this->parseItem($item);
        }
    }

    /**
     * @param  array  $item
     *
     * @return array
     */
    private function parseItem($item)
    {
        $item = (array)$item;

        static $haveDotsProperties;

        if (is_null($haveDotsProperties)) {
            $haveDotsProperties = false;
            foreach (array_keys($item) as $field) {
                if (strpos('.', $field) !== false) {
                    $haveDotsProperties = true;
                    break;
                }
            }
        }

        if (!$haveDotsProperties) {
            return $item;
        }

        static $map;

        if (!$map) {
            $map = [];
            $values = array_keys($item);
            $i = 0;
            foreach ($item as $key => $value) {
                data_set($map, $key, $values[$i]);
                $i++;
            }
        }

        return dotsToArray($item, $map);
    }

    /**
     * @return bool
     */
    protected function multiThreading(): bool
    {
        return $this->threadCount > 1;
    }

    /**
     * @return string
     */
    protected function idField(): string
    {
        return $this->entity->id();
    }

    /**
     * This data will be loaded on start or reloaded every bulk if $bulkSelection = true
     */
    abstract protected function initData(): void;

    /**
     * This data will be reloaded every bulk
     */
    abstract protected function bulkData(): void;

    /**
     * Transform document
     *
     * @param  array  $item
     *
     * @return array
     */
    abstract protected function item(array $item): array;

    /**
     * Query to get data
     *
     * @return Builder
     */
    abstract public function mainQuery(): Builder;

    /**
     * Get entity object
     *
     * @return Entity
     */
    abstract protected function entity(): Entity;
}
