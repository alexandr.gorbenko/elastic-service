<?php


namespace App\Services\Search\Entities;


use App\Services\Search\Entity;

class Product extends Entity
{
    /**
     * Entity index
     */
    const _INDEX_ = 'product';
    /**
     * Entity type
     */
    const _TYPE_ = 'product';


    /**
     * @integer ID field
     */
    const ID = 'product_id';
    /**
     * @integer Generic ID field
     */
    const CATEGORY = 'category_id';
    /**
     * @integer Brand ID field
     */
    const BRAND = 'brand_id';
    /**
     * @string Main article number field
     */
    const NAME = 'name';
    /**
     * @string EAN field
     */
    const DESCRIPTION = 'description';
    /**
     * @double Price field
     */
    const PRICE = 'price';
    /**
     * @boolean State field. 1 - in stock, 0 - not in stock
     */
    const STATE_STOCK = 'state_stock';
}