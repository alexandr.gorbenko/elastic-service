<?php


namespace App\Services\Search;


use App\Services\Search\Contracts\RequestContract;
use App\Services\Search\Facades\Search;

class RequestBag
{
    /**
     * @var array
     */
    protected $requests;

    /**
     * @param  RequestContract  $request
     *
     * @param  null  $key
     *
     * @return $this
     */
    public function add(RequestContract $request, $key = null): self
    {
        $this->normalizeRequest($request);

        if (is_null($key)) {
            $this->requests[] = $request;
        } else {
            $this->requests[$key] = $request;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getRequests(): array
    {
        return $this->requests;
    }

    /**
     * @param  RequestContract  $request
     */
    private function normalizeRequest(RequestContract &$request)
    {
        if(!$request->isInitialized()) {
            $request = Search::request($request);
        }
    }
}