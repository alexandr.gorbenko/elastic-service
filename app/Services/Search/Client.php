<?php


namespace App\Services\Search;

use App\Services\Search\Contracts\ClientContract;
use App\Services\Search\Contracts\RequestContract;
use App\Services\Search\Requests\MultiRequest;
use App\Services\Search\Requests\SingleRequest;
use Elasticsearch\Client as ElasticEngineClient;

class Client implements ClientContract
{
    /**
     * @var ElasticEngineClient
     */
    private $engineClient;
    /**
     * @var array
     */
    private $parameters;

    /**
     * Search constructor
     *
     * @param  ElasticEngineClient  $engineClient
     */
    public function __construct(ElasticEngineClient $engineClient)
    {
        $this->engineClient = $engineClient;
        $this->parameters = [];
    }

    /**
     * Set global parameters (first usage in SearchServiceProvider)
     *
     * @param  array  $parameters
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
    }

    public function version()
    {
        return config('app.version');
    }

    /**
     * Get search engine client
     *
     * @return ElasticEngineClient
     */
    public function engine(): ElasticEngineClient
    {
        return $this->engineClient;
    }

    /**
     * Get search instance by name
     *
     * @param  string  $name
     *
     * @return Client
     * @throws \Exception
     */
    public function instance(string $name = null): self
    {
        if (is_null($name)) {
            return $this;
        }

        $types = app()['config']->get('search.types');

        if (!isset($types[$name])) {
            throw new \Exception("Client '$name' doesn't exists");
        }

        return app("search.$name");
    }

    /**
     * Create single search request
     *
     * @param  SingleRequest  $request
     * @param  array  $parameters
     *
     * @return SingleRequest
     */
    public function singleRequest(SingleRequest $request, array $parameters = []): SingleRequest
    {
        return $this->request($request, $parameters);
    }

    /**
     * Create request with multiple searches
     *
     * @param  MultiRequest  $request
     * @param  array  $parameters
     *
     * @return MultiRequest
     */
    public function multiRequest(MultiRequest $request, array $parameters = []): MultiRequest
    {
        return $this->request($request, $parameters);
    }

    /**
     * Create any search request
     *
     * @param  RequestContract  $request
     * @param  array  $parameters
     *
     * @return Request
     */
    public function request(RequestContract $request, array $parameters = []): RequestContract
    {
        if (!$request->isInitialized()) {
            $request->setParameters(array_replace($this->parameters, $parameters));
            $request->init();
            $request->postInit();
        }

        return $request;
    }

    /**
     * Execute request and return result
     *
     * @param  RequestContract  $request
     * @param  array  $parameters
     *
     * @return array
     */
    protected function execute(RequestContract $request, array $parameters = []): array
    {
        return $this->request($request, $parameters)
            ->execute()
            ->toArray();
    }

    /**
     * @param $name
     * @param $arguments
     */
    public function __call($name, $arguments)
    {
        abort(404);
    }
}
