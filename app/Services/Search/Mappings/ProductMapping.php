<?php


namespace App\Services\Search\Mappings;


use App\Services\Search\Entities\Product;
use App\Services\Search\Enums\SearchProperty;
use App\Services\Search\Mapping;
use App\Services\Search\Traits\TMappingProperties;

class ProductMapping extends Mapping
{
    use TMappingProperties;

    /**
     * Array of additional search properties
     *
     * @var array
     */
    public $searchProperties = [
        SearchProperty::ENABLE,
        SearchProperty::WEIGHT,
    ];

    /**
     * Index type
     *
     * @return string
     */
    public function type(): string
    {
        return Product::_TYPE_;
    }

    /**
     * Document mapping
     *
     * @return array
     */
    public function mapping(): array
    {
        return [
            Product::ID => $this->integer(),

            Product::CATEGORY => $this->integer(),
            Product::BRAND => $this->integer(),

            Product::NAME => $this->keyword([
                Mapping::FIELD_AUTOCOMPLETE => $this->autocomplete()
            ]),

            Product::DESCRIPTION => $this->field(Mapping::TYPE_TEXT, Mapping::ANALYZER_NUMBERS_ONLY, [], [
                Mapping::FIELD_AUTOCOMPLETE => $this->autocomplete()
            ]),

            Product::PRICE => $this->double(),

            Product::STATE_STOCK => $this->boolean(),

        ];
    }

}
