<?php


namespace App\Services\Search\Traits;


use Illuminate\Support\Arr;

trait TParameters
{
    /**
     * @var array
     */
    protected $parameters;

    /**
     * Set parameters
     *
     * @param  array  $parameters
     */
    public function setParameters(array $parameters = []): void
    {
        $this->parameters = $parameters;
    }

    /**
     * Get array of request parameters
     *
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * Get request parameter using "dot" notation
     *
     * @param  string  $name
     * @param  string|null  $default
     *
     * @return string|null|array
     */
    public function get(string $name, $default = null)
    {
        return $this->parameters[$name] ?? Arr::get($this->parameters, $name) ?? $default;
    }

    /**
     * Set request parameter using "dot" notation
     *
     * @param  string  $name
     * @param $value
     *
     * @return self
     */
    public function set(string $name, $value): self
    {
        data_set($this->parameters, $name, getValue($value));

        return $this;
    }

    /**
     * Add request parameter using "dot" notation
     *
     * @param  string  $name
     * @param $value
     *
     * @param  bool  $returnData
     *
     * @return self|array
     */
    public function add(string $name, $value, $returnData = false)
    {
        $data = $this->get($name);
        if (is_null($data)) {
            $data = [];
        }
        if (!is_array($data)) {
            $data = [$data];
        }
        $data[] = getValue($value);

        $this->set($name, $data);

        if ($returnData) {
            return $data;
        }

        return $this;
    }

    /**
     * @param  string  $name
     * @param $default
     *
     * @return self
     */
    public function replace(string $name, $default)
    {
        return $this->set($name, array_replace_recursive($default, $this->get($name) ?? []));
    }
}
