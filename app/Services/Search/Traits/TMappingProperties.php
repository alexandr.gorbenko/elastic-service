<?php


namespace App\Services\Search\Traits;


use App\Services\Search\Mapping;

trait TMappingProperties
{
    /**
     * @param  string  $type
     * @param  string|null  $analyzer
     *
     * @param  array  $properties
     * @param  array  $fields
     *
     * @return array
     */
    protected function field(
        string $type,
        ?string $analyzer = Mapping::ANALYZER_DEFAULT,
        array $properties = [],
        array $fields = []
    ): array {
        $array = [
            'type' => $type
        ];

        if (!empty($analyzer)) {
            $array['analyzer'] = $analyzer;
        }

        $array = array_merge($array, $properties);

        if (!empty($fields)) {
            $array['fields'] = $fields;
        }

        return $array;
    }

    /**
     * @param  array  $fields
     *
     * @return array
     */
    protected function boolean(array $fields = []): array
    {
        return $this->field(Mapping::TYPE_BOOLEAN, null, [], $fields);
    }

    /**
     * @param  array  $fields
     *
     * @return array
     */
    protected function integer(array $fields = []): array
    {
        return $this->field(Mapping::TYPE_INTEGER, null, [], $fields);
    }

    /**
     * @param  array  $fields
     *
     * @return array
     */
    protected function double(array $fields = []): array
    {
        return $this->field(Mapping::TYPE_DOUBLE, null, [], $fields);
    }

    /**
     * @param  array  $fields
     *
     * @return array
     */
    protected function keyword(array $fields = []): array
    {
        return $this->field(Mapping::TYPE_TEXT, Mapping::ANALYZER_KEYWORD, [], $fields);
    }

    /**
     * @param  array  $fields
     *
     * @return array
     */
    protected function text(array $fields = []): array
    {
        return $this->field(Mapping::TYPE_TEXT, Mapping::ANALYZER_DEFAULT, [], $fields);
    }

    /**
     * @param  array  $fields
     *
     * @return array
     */
    protected function cleanText(array $fields = []): array
    {
        return $this->field(Mapping::TYPE_TEXT, Mapping::ANALYZER_CLEAN_TEXT, [], $fields);
    }

    /**
     * @return array
     */
    protected function autocomplete(): array
    {
        return $this->field(
            Mapping::TYPE_TEXT,
            Mapping::ANALYZER_AUTOCOMPLETE,
            ['search_analyzer' => Mapping::ANALYZER_DEFAULT]
        );
    }


    protected function fields(array $fields)
    {
        return ['properties' => $fields];
    }

    /**
     * @param  array  $properties
     *
     * @return array
     */
    protected function nested(array $properties)
    {
        return [
            'type' => self::TYPE_NESTED,
            'include_in_parent' => true,
            'properties' => $properties
        ];
    }

    /**
     * Create multi-lang mapping structure
     *
     * @param  array  $fields
     *
     * @return array
     */
    protected function multiLang(array $fields = [])
    {
        $properties = [];

        foreach (array_keys($this->languagesMap()) as $lang) {
            $properties[$lang] = [
                'type' => Mapping::TYPE_TEXT,
                'analyzer' => $this->analyzer($lang, Mapping::ANALYZER_DEFAULT),
            ];
            if (!empty($fields)) {
                foreach ($fields as $field => &$settings) {
                    $settings['analyzer'] = $this->analyzer($lang, $settings['analyzer']);
                }
                $properties[$lang] = array_merge($properties[$lang], ['fields' => $fields]);
            }
        }

        return ['properties' => $properties];
    }

    protected function nestedMultiLang(array $fields = [])
    {
        $properties = [];

        foreach (array_keys($this->languagesMap()) as $lang) {
            $properties[$lang] = $this->nested($fields);
        }

        return ['properties' => $properties];
    }

    /**
     * Get language analyzer for every type
     *
     * @param  string  $lang
     * @param  string  $type
     *
     * @return string
     */
    private function analyzer(string $lang, string $type): string
    {
        return isset($this->analyzers()[$lang.'_'.$type]) ? $lang.'_'.$type : $type;
    }
}