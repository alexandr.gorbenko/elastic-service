<?php


namespace App\Services\Search\Traits;


use App\Services\Search\Query;

trait TQueryBuilder
{
    /**
     * Create new query
     *
     * @param  Query  $query
     * @param  array  $parameters
     *
     * @return Query
     */
    public function createQuery(Query $query, array $parameters = []): Query
    {
        $query->setParameters(array_replace($this->parameters, $parameters));
        $query->init();

        return $query;
    }

    /**
     * Create new query and compile it to array
     *
     * @param  Query  $query
     * @param  array  $parameters
     *
     * @return array
     */
    public function compileQuery(Query $query, array $parameters = []): array
    {
        return $this->createQuery($query, $parameters)->compile();
    }
}