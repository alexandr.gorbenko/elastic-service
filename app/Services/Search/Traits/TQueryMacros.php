<?php


namespace App\Services\Search\Traits;


use App\Services\Search\Enums\QueryTerm;
use App\Services\Search\Expressions\TermsExpression;

trait TQueryMacros
{
    /**
     * @param $query
     *
     * @return $this
     */
    public function query($query): self
    {
        return $this->set('query', $query);
    }

    /**
     * Fields to search
     *
     * @param $fields
     *
     * @return $this
     */
    public function fields($fields = []): self
    {
        $fields = is_array($fields) ? $fields : func_get_args();

        return $this->set('fields', $fields);
    }

    /**
     * Settings operator. Have 2 sates: 'and' - match all words;  'or' - match any word
     *
     * @param  string  $operator
     *
     * @return $this
     */
    public function operator(string $operator): self
    {
        return $this->set('operator', $operator);
    }

    /**
     * Match algorithm
     *
     * @param  string  $type
     *
     * @return $this
     */
    public function type(string $type): self
    {
        return $this->set('type', $type);
    }

    /**
     * Mistakes algorithm. Can be from 0 to 2 or 'auto'. Larger - more mistakes allow
     *
     * @param  string  $fuzziness
     *
     * @return $this
     */
    public function fuzziness(string $fuzziness): self
    {
        return $this->set('fuzziness', $fuzziness);
    }

    /**
     * Only for Function Score query
     *
     * @param  string  $name
     * @param $value
     *
     * @return mixed
     */
    public function params(string $name, $value)
    {
        return $this->set("params.$name", $value);
    }

    /**
     * Add terms to query filter
     *
     * @param  TermsExpression  $expression
     * @param  string  $term
     *
     * @return $this
     */
    public function where(TermsExpression $expression, string $term): self
    {
        $this->add($term, $expression);

        return $this;
    }

    /**
     * Add 'must' terms to query filter
     *
     * @param  TermsExpression  $expression
     *
     * @return $this
     */
    public function whereMust(TermsExpression $expression): self
    {
        return $this->where($expression, QueryTerm::MUST);
    }

    /**
     * Add 'should' terms to query filter
     *
     * @param  TermsExpression  $expression
     *
     * @return $this
     */
    public function whereShould(TermsExpression $expression): self
    {
        return $this->where($expression, QueryTerm::SHOULD);
    }

    /**
     * Add 'must_not' terms to query filter
     *
     * @param  TermsExpression  $expression
     *
     * @return $this
     */
    public function whereMustNot(TermsExpression $expression): self
    {
        return $this->where($expression, QueryTerm::MUST_NOT);
    }
}