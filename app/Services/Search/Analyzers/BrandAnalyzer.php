<?php


namespace App\Services\Search\Analyzers;


use App\Services\Search\Analyzer;
use App\Services\Search\AnalyzerRules\LevenshteinAnalyzerRule;
use App\Services\Search\AnalyzerRules\MinLengthAnalyzerRule;

class BrandAnalyzer extends Analyzer
{
    /**
     * Rules
     *
     * @return array
     */
    public function rules(): array
    {
        $modifyQueryString = strlen($this->query->get('query')) > 3;

        return [
            new MinLengthAnalyzerRule(3),
            new LevenshteinAnalyzerRule(1, $modifyQueryString),
        ];
    }
}