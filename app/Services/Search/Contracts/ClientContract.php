<?php


namespace App\Services\Search\Contracts;


use Elasticsearch\Client as SearchEngineClient;

interface ClientContract
{
    /**
     * Get search engine client
     *
     * @return SearchEngineClient
     */
    public function engine(): SearchEngineClient;
}
