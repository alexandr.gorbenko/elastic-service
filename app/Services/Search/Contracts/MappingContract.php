<?php


namespace App\Services\Search\Contracts;


interface MappingContract
{
    /**
     * Index type
     *
     * @return string
     */
    public function type(): string;

    /**
     * Document mapping
     *
     * @return array
     */
    public function mapping(): array;

}
