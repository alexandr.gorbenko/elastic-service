<?php


namespace App\Services\Search\Contracts;


interface ResponseContract
{
    /**
     * Set Request contract
     *
     * @param  RequestContract  $request
     */
    public function setRequest(RequestContract $request): void;

    /**
     * Set Response type
     *
     * @param  int  $type
     */
    public function setType(int $type): void;

    /**
     * Set Response parameters
     *
     * @param  array  $parameters
     */
    public function setParameters(array $parameters): void;

    /**
     * Initialize response
     */
    public function init(): void;

    /**
     * @return bool
     */
    public function hasResults(): bool;


    /**
     * Format response to array
     *
     * @return array
     */
    public function toArray(): array;

}
