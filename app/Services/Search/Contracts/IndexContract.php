<?php


namespace App\Services\Search\Contracts;


interface IndexContract
{
    /**
     * Index name
     *
     * @return string
     */
    public function index(): string;

    /**
     * Mapping
     *
     * @return MappingContract
     */
    public function mapping(): MappingContract;

    /**
     * Data Provider
     *
     * @return DataProviderContract
     */
    public function dataProvider(): DataProviderContract;

}
