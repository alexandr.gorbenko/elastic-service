<?php


namespace App\Services\Search\Contracts;


use App\Services\Search\Entity;

interface DataProviderContract
{
    /**
     * Set thread parameters
     *
     * @param  int  $thread
     * @param  int  $threadCount
     */
    public function setThreadParameters(int $thread = 1, int $threadCount = 1): void;

    /**
     * Init method
     */
    public function init(): void;

    /**
     * Get array of items
     *
     * @return array
     */
    public function currentItems(): array;

    /**
     * Total items
     *
     * @return int
     */
    public function total(): int;

    /**
     * Get entity object
     *
     * @return Entity
     */
    public function getEntity(): Entity;

    /**
     * Set specific ids
     *
     * @param  array  $ids
     */
    public function filterIds(array $ids): void;
}