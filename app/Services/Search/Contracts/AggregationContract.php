<?php


namespace App\Services\Search\Contracts;


interface AggregationContract extends ExpressionContract
{
    /**
     * Aggregation name
     *
     * @return string
     */
    public function name(): string;
}