<?php


namespace App\Services\Search\Contracts;


use App\Services\Search\Query;

interface AnalyzerContract
{
    /**
     * @param  AnalyzerRuleContract  $rule
     *
     * @return mixed
     */
    public function addRule(AnalyzerRuleContract $rule);

    /**
     * @return Query
     */
    public function getQuery(): Query;

    /**
     * @param  array  $items
     *
     * @return mixed
     */
    public function setItems(array $items);

    /**
     * @return array
     */
    public function items(): array;

    /**
     * @return bool
     */
    public function succeed(): bool;

    /**
     * @return bool
     */
    public function fails(): bool;

    /**
     * Execute
     */
    public function analyze();

}