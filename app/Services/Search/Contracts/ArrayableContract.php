<?php


namespace App\Services\Search\Contracts;


interface ArrayableContract
{

    public function toArray(): array;

}