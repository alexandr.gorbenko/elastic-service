<?php


namespace App\Services\Search\Contracts;


interface SortContract extends ArrayableContract
{
    /**
     * @return ExpressionContract
     */
    public function getExpression(): ExpressionContract;

}
