<?php


namespace App\Services\Search\Contracts;


interface RequestContract extends ExecutableContract
{
    /**
     * Initialize method
     */
    public function init(): void;

    /**
     * Switch request to initialized status
     */
    public function postInit(): void;

    /**
     * Initialization status
     *
     * @return bool
     */
    public function isInitialized(): bool;

    /**
     * Set parameters to request
     *
     * @param  array  $parameters
     *
     * @return void
     */
    public function setParameters(array $parameters = []): void;


    /**
     * Execute request and get response
     *
     * @return ResponseContract
     */
    public function execute(): ResponseContract;


    /**
     * Get response class name
     *
     * @return ResponseContract
     */
    public function response(): ResponseContract;
}
