<?php


namespace App\Services\Search\Contracts;


interface AnalyzerRuleContract
{
    /**
     * @param  AnalyzerContract  $analyzer
     *
     * @return mixed
     */
    public function setAnalyzer(AnalyzerContract $analyzer);

    /**
     * Execute rule
     */
    public function execute();

}