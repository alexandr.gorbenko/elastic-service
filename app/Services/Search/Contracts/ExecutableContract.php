<?php


namespace App\Services\Search\Contracts;


interface ExecutableContract
{

    public function execute();

}