<?php


namespace App\Services\Search\Contracts;


interface QueryContract
{
    /**
     * Set parameters to query
     *
     * @param  array  $parameters
     *
     * @return self
     */
    public function setParameters(array $parameters = []);

    /**
     * Required parameters
     *
     * @return array
     */
    public function required(): array;

    /**
     * Compile query
     *
     * @return array
     */
    public function compile(): array;
}
