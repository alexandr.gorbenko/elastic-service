<?php


namespace App\Services\Search\Facades;


use App\Services\Search\Contracts\RequestContract;
use App\Services\Search\Requests\MultiRequest;
use App\Services\Search\Requests\SingleRequest;
use Elasticsearch\Client as SearchClient;
use Illuminate\Support\Facades\Facade;

/**
 * @method static string version()
 * @method static SearchClient engine()
 * @method static self instance(string $class)
 * @method static RequestContract request(RequestContract $request, array $parameters = [])
 * @method static SingleRequest singleRequest(SingleRequest $request, array $parameters = [])
 * @method static MultiRequest multiRequest(MultiRequest $request, array $parameters = [])
 * @method static array execute(RequestContract $request, array $parameters = [])
 *
 */
class Search extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'search';
    }

}
