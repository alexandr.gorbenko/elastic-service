<?php


namespace App\Services\Search;


use App\Services\Search\Contracts\DataProviderContract;
use App\Services\Search\Contracts\IndexContract;
use App\Services\Search\Enums\SearchProperty;
use App\Services\Search\Facades\Search as SearchService;
use App\Services\Search\Helpers\Memory;
use App\Services\Search\Requests\Base\IdsRequest;
use Illuminate\Console\OutputStyle;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

abstract class Index implements IndexContract
{
    /**
     * Current index
     *
     * @var string
     */
    protected $index;
    /**
     * Current mapping
     *
     * @var Mapping
     */
    protected $mapping;
    /**
     * @var DataProviderContract
     */
    protected $dataProvider;
    /**
     * @var OutputStyle
     */
    protected $output;
    /**
     * @var int
     */
    protected $thread;
    /**
     * @var array
     */
    protected $parameters;
    /**
     * @var string
     */
    protected $updatingSuffix = '_updating-';
    /**
     * How much documents will be sent to search engine per request
     *
     * @var int
     */
    public $bulkSize = 1000;
    /**
     * @var int
     */
    public $threadCount = 1;

    /**
     * Index constructor.
     *
     * @param  OutputStyle  $output
     * @param  int  $thread
     * @param  array  $parameters
     */
    public function __construct(OutputStyle $output, int $thread = 1, array $parameters = [])
    {
        ini_set('memory_limit', -1);

        $this->output = $output;
        $this->thread = $thread;
        $this->parameters = $parameters;

        $this->output->write('Loading mapping... ');
        $this->mapping = $this->mapping();
        $this->output->writeln('Done');
    }

    /**
     * Init Data Provider
     */
    protected function initDataProvider(): void
    {
        $this->output->write('Loading entities... ');
        $this->dataProvider = $this->dataProvider();
        $this->dataProvider->setThreadParameters($this->thread, $this->threadCount);
        $this->dataProvider->init();
        $this->output->writeln('Done');
    }

    /**
     * Creating index
     */
    public function create(): void
    {
        $this->index = getIndexName($this->index()).'-'.date('Y_m_d-His');

        SearchService::engine()->indices()->create([
            'index' => $this->index,
            'body' => $this->mapping->body()
        ]);

        $this->switchUpdating();

        $this->output->text('Created index: <info>'.$this->index.'</info>');
    }

    /**
     * Main command to start reindex action
     *
     */
    public function reindex(): void
    {
        $this->index = $this->getUpdating();

        if (!$this->index || !$this->exists()) {
            $this->output->error('You must create index first. Use "search:create-index" command');
            exit;
        }

        $this->initDataProvider();

        $this->pushData();
        $this->beforeComplete();
        $this->switchActive();
        Memory::clear();
    }

    /**
     * Pushing data to new index
     */
    protected function pushData(): void
    {
        $data = [];
        $i = 0;

        $this->output->text('Pushing data progress: ');

        $total = (int)ceil($this->dataProvider->total() / $this->bulkSize);

        $this->output->progressStart($total);

        $time = microtime(true);

        while ($items = $this->dataProvider->currentItems()) {
            foreach ($items as $item) {
                $i++;

                $this->indexItem($data, $item);

                if ($i % $this->bulkSize == 0) {
                    $this->bulk($data);
                    $this->output->progressAdvance();
                }
            }
        }

        $this->bulk($data);

        $time = date('H:i:s', microtime(true) - $time);

        $this->output->progressFinish();
        $this->output->text("Finished in: <info>$time</info>");
        $this->output->text(str_repeat('-', 50));
        $this->output->newLine();
    }

    /**
     * Main command to start update action
     */
    public function update(): void
    {
        $this->index = $this->getActive();

        if (!$this->index) {
            $this->output->error('Index not found.');
            exit;
        }

        $this->initDataProvider();

        $this->updateData();
        $this->beforeComplete();
        Memory::clear();
    }

    /**
     * Updating data in current index
     *
     */
    protected function updateData(): void
    {
        $data = [];
        $i = 0;

        $this->dataProvider->filterIds($this->getUpdateIds());

        while ($items = $this->dataProvider->currentItems()) {
            $databaseIds = array_map(function ($item) {
                return $this->itemId($item);
            }, $items);

            $elasticIds = array_flip($this->getIds($databaseIds));

            $databaseIds = array_flip($databaseIds);

            $update = array_intersect_key($databaseIds, $elasticIds);

            $create = array_flip(array_filter(array_flip($databaseIds), function ($id) use ($elasticIds) {
                return !isset($elasticIds[$id]);
            }));

            $delete = array_flip(array_filter(array_flip($elasticIds), function ($id) use ($databaseIds) {
                return !isset($databaseIds[$id]);
            }));

            foreach ($items as $item) {
                $i++;

                $itemId = $this->itemId($item);

                if (isset($update[$itemId])) {
                    $this->updateItem($data, $item);
                } elseif (isset($create[$itemId])) {
                    $this->createItem($data, $item);
                } elseif (isset($delete[$itemId])) {
                    $this->deleteItem($data, $item);
                }


                if ($i % $this->bulkSize == 0) {
                    $this->bulk($data);
                }
            }
        }

        $this->bulk($data);
    }

    /**
     * Format item to push
     *
     * @param  array  $data
     * @param  array  $item
     */
    protected function indexItem(array &$data, array $item): void
    {
        $data['body'][] = [
            'index' => $this->itemMeta($item)
        ];
        $data['body'][] = $this->itemDocument($item);
    }

    /**
     * Format item to create
     *
     * @param  array  $data
     * @param  array  $item
     */
    protected function createItem(array &$data, array $item): void
    {
        $data['body'][] = [
            'create' => $this->itemMeta($item)
        ];
        $data['body'][] = $this->itemDocument($item);
    }

    /**
     * Format item to update
     *
     * @param  array  $data
     * @param  array  $item
     */
    protected function updateItem(array &$data, array $item): void
    {
        $data['body'][] = [
            'update' => $this->itemMeta($item)
        ];
        $data['body'][] = [
            'doc' => $this->itemDocument($item)
        ];
    }

    /**
     * Format item to delete
     *
     * @param  array  $data
     * @param  array  $item
     */
    protected function deleteItem(array &$data, array $item): void
    {
        $data['body'][] = [
            'delete' => $this->itemMeta($item)
        ];
    }

    /**
     * Before complete
     */
    protected function beforeComplete(): void
    {
    }

    /**
     * Get ids from exists documents in search engine
     *
     * @param  array  $ids
     *
     * @return array
     */
    protected function getIds(array $ids): array
    {
        return SearchService::singleRequest(new IdsRequest(), [
            '_index' => $this->index,
            'size' => Request::MAX_SIZE,
            'type' => $this->mapping->type(),
            'ids' => $ids
        ])->execute()->ids();
    }

    /**
     * @return array
     */
    protected function getUpdateIds(): array
    {
        return DB::table('pkwteile_store.elastic_update')
            ->select('entityId')
            ->where('entityType', $this->dataProvider->getEntity()->type())
            ->pluck('entityId')
            ->all();
    }

    /**
     * Send data to search engine
     *
     * @param $data
     */
    protected function bulk(&$data): void
    {
        if (!empty($data)) {
            SearchService::engine()->bulk($data);
            $data = [];
        }
    }

    /**
     * Format item id for search engine
     *
     * @param $item
     *
     * @return string
     */
    protected function itemId($item): string
    {
        return $item[$this->dataProvider->getEntity()->id()];
    }

    /**
     * Format item meta for search engine
     *
     * @param $item
     *
     * @return array
     */
    protected function itemMeta($item): array
    {
        return [
            '_id' => $this->itemId($item),
            '_index' => $this->index,
        ];
    }

    /**
     * Populate document with search properties default values
     *
     * @param  array  $document
     *
     * @return array
     */
    protected function itemDocument(array $document)
    {
        SearchProperty::populateDocument($document, $this->mapping->searchProperties);

        return $document;
    }

    /**
     * Check if index is exists
     *
     * @return bool
     */
    protected function exists(): bool
    {
        return SearchService::engine()->indices()->exists(['index' => $this->index]);
    }

    /**
     * Check if alias has active index
     *
     * @return bool
     */
    public function hasActive(): bool
    {
        return !empty($this->getActive());
    }

    /**
     * Get current active index
     *
     * @return string
     */
    protected function getActive(): ?string
    {
        return $this->getAlias(getIndexName($this->index()));
    }

    /**
     * Get current updating index
     *
     * @return string|null
     */
    protected function getUpdating(): ?string
    {
        $alias = getIndexName($this->index()).$this->updatingSuffix.$this->thread;
        $index = $this->getAlias($alias);

        if (!$index) {
            $this->output->error('There is no active updating index for thread '.$this->thread);
            exit;
        }

        SearchService::engine()->indices()->updateAliases([
            'body' => [
                'actions' => [
                    ['remove' => ['index' => $index, 'alias' => $alias]]
                ]
            ]
        ]);

        return $index;
    }

    protected function hasUpdating(): bool
    {
        $indexes = SearchService::engine()->indices()->getAlias();
        foreach ($indexes as $index => $params) {
            foreach ($params['aliases'] as $alias => $values) {
                if (strstr($alias, getIndexName($this->index()).$this->updatingSuffix)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param  string  $name
     *
     * @return string|null
     */
    private function getAlias(string $name): ?string
    {
        return Arr::first($this->getAliases($name));
    }

    /**
     * @param  string  $name
     *
     * @return array
     */
    private function getAliases(string $name): array
    {
        $aliases = [];

        $indexes = SearchService::engine()->indices()->getAlias();
        foreach ($indexes as $index => $params) {
            if (array_key_exists($name, $params['aliases'])) {
                $aliases[] = $index;
            }
        }

        return $aliases;
    }

    protected function switchUpdating()
    {
        $alias = getIndexName($this->index()).$this->updatingSuffix;

        $remove = [];
        for ($i = 1; $i <= $this->threadCount; $i++) {
            $remove = array_merge($remove, $this->getAliases($alias.$i));
        }

        $actions = [];
        foreach ($remove as $index) {
            $actions[] = ['remove' => ['index' => $index, 'alias' => $alias.'*']];
        }


        for ($i = 1; $i <= $this->threadCount; $i++) {
            $actions[] = ['add' => ['index' => $this->index, 'alias' => $alias.$i]];
        }

        SearchService::engine()->indices()->updateAliases(['body' => ['actions' => $actions]]);
    }

    /**
     * Switch alias to new index
     */
    protected function switchActive(): void
    {
        if ($this->hasUpdating()) {
            return;
        }

        $alias = getIndexName($this->index());
        $remove = $this->getAliases(getIndexName($this->index()));

        $actions = [];
        foreach ($remove as $index) {
            $actions[] = ['remove' => ['index' => $index, 'alias' => $alias]];
        }

        $actions[] = ['add' => ['index' => $this->index, 'alias' => $alias]];

        SearchService::engine()->indices()->updateAliases(['body' => ['actions' => $actions]]);
    }
}
