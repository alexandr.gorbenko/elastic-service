<?php


namespace App\Services\Search;

use Exception as BaseException;
use Illuminate\Http\JsonResponse;

abstract class Exception extends BaseException
{
    /**
     * @param  \Laravel\Lumen\Http\Request  $request
     *
     * @return JsonResponse
     */
    public function render(\Laravel\Lumen\Http\Request $request)
    {
        return new JsonResponse(
            [
                'success' => false,
                'code' => $this->code(),
                'message' => $this->getMessage(),
            ],
            500,
            [],
            JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
        );
    }

    abstract function code(): int;
}