<?php


namespace App\Services\Search\Providers;


use App\Services\Auth\Exceptions\TokenFileNotFoundException;
use App\Services\Auth\Exceptions\WrongTokenException;
use Elasticsearch\Client as ElasticClient;
use Elasticsearch\ClientBuilder as ElasticClientBuilder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Http\Request;

class SearchServiceProvider extends ServiceProvider
{

    protected $services;

    public function register()
    {
        $this->initServices();
        $this->registerSearchEngine();
        $this->registerClients();
    }

    public function boot()
    {
        $this->switchClient();
    }

    protected function initServices()
    {
        $clients = $this->app['config']->get('search.clients');
        foreach ($clients as $alias => $client) {
            $this->services[$alias] = $client['class'];
        }
    }


    protected function registerSearchEngine()
    {
        $this->app->singleton(ElasticClient::class, function ($app) {
            return ElasticClientBuilder::create()
                ->setHosts(explode(' ', $app['config']->get('services.elastic.hosts')))
                ->build();
        });
    }

    protected function registerClients()
    {
        $elasticClient = $this->app->make(ElasticClient::class);

        foreach ($this->services as $type => $class) {
            $this->app->singleton('search.'.$type, function () use ($class, $elasticClient) {
                return new $class($elasticClient);
            });
        }
    }

    protected function switchClient()
    {
        /**
         * @var Request $request
         */
        $request = $this->app->has('request') ? $this->app->get('request') : null;

        $client = 'guest';

        if ($request->has('token')) {
            $client = $this->getCurrentClient();
        }

        $this->app->currentClient = $client;
        $this->app->alias("search.{$client}", 'search');

        $parameters = !empty($request) ? $request->all() : [];
        $this->app->get('search')->setParameters($parameters);
    }

    protected function getCurrentClient()
    {
        if (App::runningInConsole()) {
            return 'console';
        }

        $tokens = $this->parseTokens();
        $token = $this->app['request']->input('token');
        $searchService = $tokens[$token] ?? null;

        if (!$searchService) {
            throw new WrongTokenException("Token '$token' is invalid");
        }

        return $searchService;
    }

    protected function parseTokens()
    {
        $fileName = '.tokens';
        $tokensFile = @fopen(base_path($fileName), 'r');

        if (!$tokensFile) {
            throw new TokenFileNotFoundException("Cannot find '$fileName' file in base path");
        }

        $tokens = [];

        while ($keyToken = fgets($tokensFile)) {
            if (strpos($keyToken, "#") === 0) {
                continue;
            }
            list($key, $token) = explode(':', $keyToken);
            $tokens[str_replace(PHP_EOL, "", $token)] = $key;
        }

        foreach ($this->app['config']->get('search.clients') as $alias => $client) {
            $tokens[$client['testToken']] = $alias;
        }

        return $tokens;
    }

}
