<?php


namespace App\Services\Search\Expressions;


use App\Services\Search\Expression;

class TermsExpression extends Expression
{
    /**
     * @var string
     */
    protected $field;
    /**
     * @var
     */
    protected $values;

    /**
     * TermsExpression constructor.
     *
     * @param  string  $field
     * @param $values
     */
    public function __construct(string $field, $values)
    {
        $this->field = $field;
        $this->values = getValue($values);

        parent::__construct($this->format());
    }

    /**
     * @return array
     */
    protected function format()
    {
        $type = is_array($this->values) ? 'terms' : 'term';

        return [
            $type => [
                $this->field => $this->values
            ]
        ];
    }
}
