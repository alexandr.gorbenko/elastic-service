<?php


namespace App\Services\Search\Expressions\Product;


use App\Services\Search\Entities\Product;
use App\Services\Search\Expressions\TermsExpression;

class CategoryTermsExpression extends TermsExpression
{
    public function __construct($values)
    {
        parent::__construct(Product::CATEGORY, $values);
    }
}