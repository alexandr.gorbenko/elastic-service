<?php


namespace App\Services\Search\Expressions\SearchProperty;


use App\Services\Search\Enums\SearchProperty;
use App\Services\Search\Expressions\TermsExpression;

class SearchEnableTermExpression extends TermsExpression
{
    /**
     * EnableTermExpression constructor.
     *
     */
    public function __construct()
    {
        parent::__construct(searchProperty(SearchProperty::ENABLE), true);
    }

}
