<?php


namespace App\Services\Search\Expressions;


use App\Services\Search\Contracts\ExpressionContract;
use App\Services\Search\Enums\QueryTerm;
use App\Services\Search\Expression;

class BoolExpression extends Expression
{
    protected $must;
    protected $should;
    protected $mustNot;

    public function __construct(
        ExpressionContract $must = null,
        ExpressionContract $should = null,
        ExpressionContract $mustNot = null
    ) {
        $this->must = $must;
        $this->should = $should;
        $this->mustNot = $mustNot;

        parent::__construct($this->format());
    }

    protected function format(): array
    {
        return [
            'bool' => [
                QueryTerm::MUST => $this->must ?? [],
                QueryTerm::SHOULD => $this->should ?? [],
                QueryTerm::MUST_NOT => $this->mustNot ?? [],
            ]
        ];
    }
}