<?php


namespace App\Services\Search;


use App\Services\Search\Contracts\RequestContract;
use App\Services\Search\Contracts\ResponseContract;
use App\Services\Search\Traits\TParameters;

abstract class Request implements RequestContract
{
    use TParameters;

    /**
     * Max count of items
     */
    const MAX_SIZE = 10000;

    /**
     * Initialization status
     *
     * @var bool
     */
    protected $initialized = false;

    /**
     * Initialize method
     */
    abstract public function init(): void;

    /**
     * Switch request to initialized status
     */
    public function postInit(): void
    {
        $this->initialized = true;
    }

    /**
     * @return bool
     */
    public function isInitialized(): bool
    {
        return $this->initialized;
    }

    /**
     * @param  int  $type
     * @param  array  $parameters
     *
     * @return ResponseContract
     */
    public function createResponse(int $type, array $parameters = []): ResponseContract
    {
        $response = $this->response();
        $response->setRequest($this);
        $response->setType($type);
        $response->setParameters($parameters);
        $response->init();

        return $response;
    }
}
