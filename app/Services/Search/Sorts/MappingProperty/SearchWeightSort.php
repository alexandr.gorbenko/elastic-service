<?php


namespace App\Services\Search\Sorts\MappingProperty;


use App\Services\Search\Enums\SearchProperty;
use App\Services\Search\Enums\SortOrder;
use App\Services\Search\Sorts\FieldSort;

class SearchWeightSort extends FieldSort
{

    public function __construct()
    {
        parent::__construct(searchProperty(SearchProperty::WEIGHT), SortOrder::DESC);
    }

}