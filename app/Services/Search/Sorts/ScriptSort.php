<?php


namespace App\Services\Search\Sorts;


use App\Services\Search\Contracts\ExpressionContract;
use App\Services\Search\Enums\SortOrder;
use App\Services\Search\Expression;
use App\Services\Search\Sort;

class ScriptSort extends Sort
{

    const LANG_PAINLESS = 'painless';

    /**
     * @var string
     */
    protected $source;
    /**
     * @var array
     */
    protected $params;
    /**
     * @var string
     */
    protected $order;
    /**
     * @var string
     */
    protected $lang;

    /**
     * ScriptSort constructor.
     *
     * @param  string  $source
     * @param  string  $order
     * @param  array  $params
     * @param  string  $lang
     */
    public function __construct(
        string $source,
        array $params = [],
        string $order = SortOrder::ASC,
        string $lang = self::LANG_PAINLESS
    ) {
        $this->source = $source;
        $this->params = $params;
        $this->order = $order;
        $this->lang = $lang;

        parent::__construct($this->expression());
    }

    /**
     * @return ExpressionContract
     */
    public function expression(): ExpressionContract
    {
        if ($this->source == '_score') {
            return (new FieldSort('_score', SortOrder::DESC))->getExpression();
        }

        return new Expression([
            '_script' => [
                'type' => 'number',
                'script' => [
                    'lang' => $this->lang,
                    'params' => $this->params,
                    'source' => $this->source,
                ],
                'order' => $this->order,
            ]
        ]);
    }


}