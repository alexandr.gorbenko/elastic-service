<?php


namespace App\Services\Search\Sorts;


use App\Services\Search\Contracts\ExpressionContract;
use App\Services\Search\Enums\SortOrder;
use App\Services\Search\Expression;
use App\Services\Search\Sort;

class FieldSort extends Sort
{
    /**
     * @var string
     */
    protected $field;
    /**
     * @var string
     */
    protected $order;

    /**
     * FieldSort constructor.
     *
     * @param  string  $field
     * @param  string  $order
     */
    public function __construct(string $field, string $order = SortOrder::ASC)
    {
        $this->field = $field;
        $this->order = $order;

        parent::__construct($this->expression());
    }

    /**
     * @return ExpressionContract
     */
    public function expression(): ExpressionContract
    {
        return new Expression([
            $this->field => [
                'order' => $this->order
            ]
        ]);
    }
}