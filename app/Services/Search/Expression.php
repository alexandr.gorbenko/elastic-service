<?php


namespace App\Services\Search;


use App\Services\Search\Contracts\ExpressionContract;

class Expression implements ExpressionContract
{
    /**
     * @var array
     */
    protected $expression;

    /**
     * Expression constructor.
     *
     * @param  array  $expression
     */
    public function __construct(array $expression = [])
    {
        $this->expression = getValue($expression);
    }

    /**
     * Get compiled expression
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->expression;
    }
}
