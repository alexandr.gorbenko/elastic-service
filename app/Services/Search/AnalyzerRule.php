<?php


namespace App\Services\Search;


use App\Services\Search\Contracts\AnalyzerContract;
use App\Services\Search\Contracts\AnalyzerRuleContract;

abstract class AnalyzerRule implements AnalyzerRuleContract
{
    /**
     * @var AnalyzerContract
     */
    protected $analyzer;

    /**
     * AnalyzerRule constructor.
     *
     * @param  AnalyzerContract  $analyzer
     */
    public function setAnalyzer(AnalyzerContract $analyzer)
    {
        $this->analyzer = $analyzer->addRule($this);
    }

}