<?php


namespace App\Services\Search;


use App\Services\Search\Contracts\RequestContract;
use App\Services\Search\Contracts\ResponseContract;
use App\Services\Search\Exceptions\ResponseTypeException;

abstract class Response implements ResponseContract
{
    /**
     * Success response
     */
    const TYPE_SUCCESS = 1;
    /**
     * Response with errors
     */
    const TYPE_ERROR = 99;

    /**
     * Response success code
     */
    const CODE_SUCCESS = 200;
    /**
     * Response code when validation fails
     */
    const CODE_ERROR_VALIDATION = 501;

    /**
     * @var RequestContract
     */
    protected $request;
    /**
     * @var int
     */
    protected $type;
    /**
     * @var array
     */
    protected $parameters;

    /**
     *
     */
    public function init(): void
    {
    }

    /**
     * @param  RequestContract  $request
     */
    public function setRequest(RequestContract $request): void
    {
        $this->request = $request;
    }

    /**
     * @param  int  $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @param  array  $parameters
     */
    public function setParameters(array $parameters): void
    {
        $this->parameters = $parameters;
    }


    /**
     * Format response to array
     *
     * @return array
     */
    public function toArray(): array
    {
        if ($this->type == self::TYPE_SUCCESS) {
            $response = $this->toSuccessArray();
        } elseif ($this->type == self::TYPE_ERROR) {
            $response = $this->toErrorArray();
        } else {
            throw new ResponseTypeException('Response type is invalid');
        }

        if (isDebugMode()) {
            $this->debug($response);

            return ['debug' => true];
        }

        return $response;
    }

    /**
     * Format response to success array
     *
     * @return array
     */
    protected function toSuccessArray(): array
    {
        return [
            'success' => true,
            'code' => self::CODE_SUCCESS,
            'data' => $this->data()
        ];
    }

    /**
     * Get response data
     *
     * @return array
     */
    abstract protected function data(): array;

    /**
     * Format response to error array
     *
     * @return array
     */
    protected function toErrorArray(): array
    {
        return [
            'success' => false,
            'code' => $this->parameters['code'],
            'message' => $this->parameters['message'],
            'errors' => $this->parameters['errors'] ?? []
        ];
    }

    /**
     * Debug message
     *
     * @param $response
     */
    protected function debug($response)
    {
        echo '<h3 style="color: chocolate">'.get_class($this).'</h3>';
        echo jsonBeautifier($response);
    }
}
