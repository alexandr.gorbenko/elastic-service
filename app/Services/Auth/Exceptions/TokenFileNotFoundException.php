<?php


namespace App\Services\Auth\Exceptions;


use Illuminate\Contracts\Filesystem\FileNotFoundException;

class TokenFileNotFoundException extends FileNotFoundException
{

}