<?php


namespace App\Services\Auth\Exceptions;


use App\Services\Search\Exception;

class WrongTokenException extends Exception
{

    function code(): int
    {
        return 600;
    }
}