<?php


namespace App\Services\Auth;


use App\Services\Auth\Exceptions\WrongTokenException;

class Token
{
    protected $token;
    protected $user;

    public function __construct(string $token)
    {
        list($user, $token) = $this->parseToken($token);
        $this->user = new User($user);
        $this->token = $token;
    }

    protected function parseToken(string $token): array
    {
        if (strpos($token, '/') === false) {
            throw new WrongTokenException("Token must look like 'username/token'");
        }

        return explode('/', $token);
    }


}