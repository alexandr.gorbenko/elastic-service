<?php

/**
 * @var Router $router
 */

use Laravel\Lumen\Routing\Router;

$router->get('/', function () use ($router) {
    return view('index');
});


$router->get('/search', ['uses' => 'BaseController@search', 'as' => 'base.search']);
$router->get('/autocomplete', ['uses' => 'BaseController@autocomplete', 'as' => 'base.autocomplete']);

// Products Controller
$router->group(['prefix' => '/products', 'docs' => true], function() use($router){
    $router->get('/search', ['uses' => 'ProductController@search', 'as' => 'products.search']);
    $router->get('/autocomplete', ['uses' => 'ProductController@autocomplete', 'as' => 'products.autocomplete']);
});

// Generics Controller
$router->group(['prefix' => '/generics'], function() use($router) {
    $router->get('/search', ['uses' => 'GenericController@search', 'as' => 'generics.search']);
    $router->get('/autocomplete', ['uses' => 'GenericController@autocomplete', 'as' => 'generics.autocomplete']);
});

// Brands Controller
$router->group(['prefix' => '/brands'], function() use($router) {
    $router->get('/search', ['uses' => 'BrandController@search', 'as' => 'brands.search']);
    $router->get('/autocomplete', ['uses' => 'BrandController@autocomplete', 'as' => 'brands.autocomplete']);
});


// Console Controller
$router->group([], function() use ($router) {

    $router->get('/reindex', ['uses' => 'ConsoleController@reindex', 'as' => 'reindex.all']);

    $router->get('/brands/reindex', ['uses' => 'ConsoleController@brand_reindex', 'as' => 'reindex.brand']);
    $router->get('/brands/update', ['uses' => 'ConsoleController@brand_update', 'as' => 'update.brand']);

    $router->get('/generics/reindex', ['uses' => 'ConsoleController@generic_reindex', 'as' => 'reindex.generic']);
    $router->get('/generics/update', ['uses' => 'ConsoleController@generic_update', 'as' => 'update.generic']);

    $router->get('/products/reindex', ['uses' => 'ConsoleController@product_reindex', 'as' => 'reindex.product']);
    $router->get('/products/update', ['uses' => 'ConsoleController@product_update', 'as' => 'update.product']);
});
