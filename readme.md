# Search service
Powered by Lumen Php Framework

## Versioning
Version structure: `x.y.z`

Ex. `1.3.15` where 
* 1 - major version 
* 3 - minor version 
* 15 - patch version

##### Major version (x) must be increased when:
* New index mapping

##### Minor version (y) must be increased when:
* Changed output structure

##### Patch version (y) must be increased when:
* Added new output (new controller or new action)
* Changed search logic

## Installing
First create `.tokens` file with structure like that:
```$xslt
service1:key1
service2:key2
```
For example:
```$xslt
test:$2y$10$d2JEyDoHOg7PrFeOx/1BE.XXRvPyFRkwCMCEJfq66/M8ZzdRckitO
```

This must be done for all service clients that you have in /config/search.php#clients

Than just install composer:
`composer install`

Installation complete.
