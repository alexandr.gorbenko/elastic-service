<?php

return [

    'enable' => env('CACHE_ENABLE', false),

    'default' => env('CACHE_DRIVER', 'file'),

    'stores' => [

        'array' => [
            'driver' => 'array'
        ],

        'file' => [
            'driver' => 'file',
            'path'   => storage_path().'/framework/cache',
        ],

        'memcached' => [
            'driver'  => 'memcached',
            'servers' => [
                [
                    'host' => env("MEMCACHED_HOST", '127.0.0.1'),
                    'port' => env("MEMCACHED_PORT", 11211),
                    'weight' => 100
                ],
            ],
        ],
    ],


];
