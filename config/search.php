<?php

use App\Services\Search\Client;
use App\Services\Search\Clients\TestClient;
use App\Services\Search\Entities\Product;
use App\Services\Search\Helpers\Params;

return [
    'clients' => [
        'guest' => [
            'class' => Client::class,
            'testToken' => ''
        ],
        'test' => [
            'class' => TestClient::class,
            'testToken' => 'test/F5jg9hjsaq1Ywp2bYW7jccsow1QTspA1YTsFP6b2zxpkySGN2eJQwn44dkv',
        ],

    ],
    'params' => [
        '_default' => [
            Params::HIGHLIGHT_TAG_OPEN => '<em>',
            Params::HIGHLIGHT_TAG_CLOSE => '</em>',
        ],

        Product::_TYPE_ => [

            Params::SIZE => 10,
            Params::AUTOCOMPLETE_SIZE => 5,

            Params::WEIGHT_MIN => 1,
            Params::WEIGHT_MAX => 1.5,

            Params::PRICE_MIN => 1,
            Params::PRICE_MAX => 1.2,
        ]
    ]
];