<?php

return [

    'elastic' => [
        'hosts' => env('ELASTIC_HOST'),
    ]

];
