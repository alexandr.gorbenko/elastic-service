<?php

return [
    'messages' => [
        'required' => 'The :attribute field is required.',
        'size' => 'The :attribute must be exactly :size characters length.',
    ]
];