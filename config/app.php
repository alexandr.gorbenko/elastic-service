<?php

return [
    'version' => '0.8.0',

    'debug' => env('APP_DEBUG', false),
];