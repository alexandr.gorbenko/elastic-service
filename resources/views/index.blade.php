<h1>Search service</h1>
<h3>Version: <?php echo \App\Services\Search\Facades\Search::version() ?></h3>
<h3>Framework:</h3>
<?php echo app()->version() ?>
<div>
    <h3>Elastic info:</h3>
    <table border="0" cellpadding="5" cellspacing="0" style="text-align: left">
        @foreach (\App\Services\Search\Facades\Search::engine()->info() as $key => $value)
            <tr>
                <th>{{ $key }}</th>
                <td>
                    @if(is_array($value))
                        <table>
                            @foreach ($value as $k => $v)
                                <tr>
                                    <td>{{ $k }}</td>
                                    <td>{{ $v }}</td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        {{ $value }}
                    @endif
                </td>
            </tr>
        @endforeach
    </table>
</div>