---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Base


Class BaseController
<!-- START_b833ef11870fa33d1409ac0625cbfae5 -->
## Search (in progress)

> Example request:

```bash
curl -X GET \
    -G "/search" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "/search"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):

```json
null
```

### HTTP Request
`GET /search`


<!-- END_b833ef11870fa33d1409ac0625cbfae5 -->

<!-- START_172adab38f87856e11a95da8f6cf2bbb -->
## Autocomplete (in progress)

> Example request:

```bash
curl -X GET \
    -G "/autocomplete" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "/autocomplete"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):

```json
null
```

### HTTP Request
`GET /autocomplete`


<!-- END_172adab38f87856e11a95da8f6cf2bbb -->

#Brands


Class BrandsController
<!-- START_17494f2011cfb5de53118cc58b3f074b -->
## Search (in progress)

> Example request:

```bash
curl -X GET \
    -G "/brands/search" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "/brands/search"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):

```json
null
```

### HTTP Request
`GET /brands/search`


<!-- END_17494f2011cfb5de53118cc58b3f074b -->

<!-- START_b7e899772f2fb93d98ef91a61bc07253 -->
## Autocomplete (in progress)

> Example request:

```bash
curl -X GET \
    -G "/brands/autocomplete" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "/brands/autocomplete"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):

```json
null
```

### HTTP Request
`GET /brands/autocomplete`


<!-- END_b7e899772f2fb93d98ef91a61bc07253 -->

#Generics


Class GenericsController
<!-- START_7fc98c864a75eb1a7b6760a30502da4b -->
## Search (in progress)

> Example request:

```bash
curl -X GET \
    -G "/generics/search" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "/generics/search"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):

```json
null
```

### HTTP Request
`GET /generics/search`


<!-- END_7fc98c864a75eb1a7b6760a30502da4b -->

<!-- START_22adb71cacbf9c8d3a23d9b59ff38448 -->
## Autocomplete (in progress)

> Example request:

```bash
curl -X GET \
    -G "/generics/autocomplete" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "/generics/autocomplete"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):

```json
null
```

### HTTP Request
`GET /generics/autocomplete`


<!-- END_22adb71cacbf9c8d3a23d9b59ff38448 -->

#Products


Class ProductsController
<!-- START_c76b8f15aade94572f8485ffea8d0e44 -->
## Search

> Example request:

```bash
curl -X GET \
    -G "/products/search" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"token":"test-atd\/F5jg9hjsaq1Ywp2bYW7jccsow1QTspA1YTsFP6b2zxpkySGN2eJQwn4","language":"de","query":"bremsscheibe"}'

```

```javascript
const url = new URL(
    "/products/search"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "token": "test-atd\/F5jg9hjsaq1Ywp2bYW7jccsow1QTspA1YTsFP6b2zxpkySGN2eJQwn4",
    "language": "de",
    "query": "bremsscheibe"
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "code": 200,
    "data": {
        "items": [
            {
                "_id": "9099107"
            },
            {
                "_id": "9099165"
            },
            {
                "_id": "9099212"
            },
            {
                "_id": "9099332"
            },
            {
                "_id": "9099905"
            },
            {
                "_id": "9100280"
            },
            {
                "_id": "7306557"
            },
            {
                "_id": "7828491"
            },
            {
                "_id": "7998920"
            },
            {
                "_id": "9099052"
            },
            {
                "_id": "9099055"
            },
            {
                "_id": "9099105"
            },
            {
                "_id": "9099232"
            },
            {
                "_id": "9099244"
            },
            {
                "_id": "9099352"
            }
        ],
        "paginator": {
            "total": 10000,
            "relation": "gte",
            "perPage": 15,
            "currentPage": 1,
            "hasNextPage": true,
            "hasPreviousPage": false,
            "lastPage": 667
        },
        "filters": {
            "all": {
                "max_price": 4300.07,
                "min_price": 0.16,
                "brand": {
                    "100579": 421,
                    "100544": 41,
                    "100446": 30,
                    "100444": 11,
                    "100443": 33,
                    "100381": 14,
                    "100379": 2,
                    "100377": 10,
                    "100376": 11,
                    "100375": 82
                },
                "generic": {
                    "82": 92197
                }
            },
            "active": {
                "max_price": 4300.07,
                "min_price": 0.16,
                "brand": [
                    100579,
                    100544,
                    100446,
                    100444,
                    100443,
                    100381,
                    100379,
                    100377,
                    100376,
                    100375
                ],
                "generic": [
                    82
                ]
            },
            "checked": {
                "generic": [],
                "brand": []
            }
        }
    }
}
```
> Example response (200):

```json
{
    "success": true,
    "code": 200,
    "data": {
        "items": [
            {
                "_id": "7936542",
                "_source": {
                    "alternatives": {
                        "oen": [
                            "SKBD-0023028",
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "17265"
                        ],
                        "manual": [
                            "SKBD-0023028"
                        ]
                    }
                }
            },
            {
                "_id": "23511",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "BS7104"
                        ],
                        "oen": [
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "17265",
                            "SKBD-0023028"
                        ],
                        "manual": [
                            "BS7104"
                        ]
                    }
                }
            },
            {
                "_id": "177569",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "C3Y025ABE"
                        ],
                        "oen": [
                            "C3Y025ABE",
                            "BS7104",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "17265",
                            "SKBD-0023028"
                        ],
                        "manual": [
                            "C3Y025ABE"
                        ]
                    }
                }
            },
            {
                "_id": "7499510",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "1815319325"
                        ],
                        "oen": [
                            "1815319325",
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "BS-8734",
                            "92230003",
                            "17265",
                            "SKBD-0023028"
                        ],
                        "manual": [
                            "1815319325"
                        ]
                    }
                }
            },
            {
                "_id": "7567080",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "BS-8734"
                        ],
                        "oen": [
                            "BS-8734",
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "92230003",
                            "17265",
                            "SKBD-0023028"
                        ],
                        "manual": [
                            "BS-8734"
                        ]
                    }
                }
            },
            {
                "_id": "7690221",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "92230003"
                        ],
                        "oen": [
                            "92230003",
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "17265",
                            "SKBD-0023028"
                        ],
                        "altArticleNo2": [
                            "92230003"
                        ],
                        "manual": [
                            "92230003"
                        ]
                    }
                }
            },
            {
                "_id": "7710382",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "17265"
                        ],
                        "oen": [
                            "17265",
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "SKBD-0023028"
                        ],
                        "manual": [
                            "17265"
                        ]
                    }
                }
            },
            {
                "_id": "7216564",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "8120 101117"
                        ],
                        "oen": [
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "17265",
                            "SKBD-0023028",
                            "8120 101117C",
                            "82B0878",
                            "61360.10"
                        ],
                        "manual": [
                            "8120 101117"
                        ]
                    }
                }
            },
            {
                "_id": "1995203",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "209325"
                        ],
                        "oen": [
                            "209325",
                            "BS7104",
                            "C3Y025ABE",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "17265",
                            "SKBD-0023028",
                            "8120 101117C",
                            "82B0878",
                            "61360.10"
                        ],
                        "manual": [
                            "209325"
                        ]
                    }
                }
            },
            {
                "_id": "7059928",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "C3009V"
                        ],
                        "oen": [
                            "C3009V",
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "17265",
                            "SKBD-0023028",
                            "8120 101117C",
                            "82B0878",
                            "61360.10"
                        ],
                        "altArticleNo2": [
                            "C3009V"
                        ],
                        "manual": [
                            "C3009V"
                        ]
                    }
                }
            }
        ],
        "paginator": {
            "total": 13,
            "relation": "eq",
            "perPage": 10,
            "currentPage": 1,
            "hasNextPage": true,
            "hasPreviousPage": false,
            "lastPage": 2
        }
    }
}
```

### HTTP Request
`GET /products/search`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `token` | string |  required  | Access token.
        `language` | string |  required  | Language code, size:2.
        `query` | string |  required  | What to search.
    
<!-- END_c76b8f15aade94572f8485ffea8d0e44 -->

<!-- START_a427d583e3851b19bb3bc1044d0b6380 -->
## Autocomplete

> Example request:

```bash
curl -X GET \
    -G "/products/autocomplete" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"token":"test-atd\/F5jg9hjsaq1Ywp2bYW7jccsow1QTspA1YTsFP6b2zxpkySGN2eJQwn4","language":"de","query":"bremsscheibe"}'

```

```javascript
const url = new URL(
    "/products/autocomplete"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "token": "test-atd\/F5jg9hjsaq1Ywp2bYW7jccsow1QTspA1YTsFP6b2zxpkySGN2eJQwn4",
    "language": "de",
    "query": "bremsscheibe"
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "code": 200,
    "data": {
        "items": [
            {
                "_id": "9099107"
            },
            {
                "_id": "9099165"
            },
            {
                "_id": "9099212"
            },
            {
                "_id": "9099332"
            },
            {
                "_id": "9099905"
            },
            {
                "_id": "9100280"
            },
            {
                "_id": "7306557"
            },
            {
                "_id": "7828491"
            },
            {
                "_id": "7998920"
            },
            {
                "_id": "9099052"
            },
            {
                "_id": "9099055"
            },
            {
                "_id": "9099105"
            },
            {
                "_id": "9099232"
            },
            {
                "_id": "9099244"
            },
            {
                "_id": "9099352"
            }
        ],
        "paginator": {
            "total": 10000,
            "relation": "gte",
            "perPage": 15,
            "currentPage": 1,
            "hasNextPage": true,
            "hasPreviousPage": false,
            "lastPage": 667
        },
        "filters": {
            "all": {
                "max_price": 4300.07,
                "min_price": 0.16,
                "brand": {
                    "100579": 421,
                    "100544": 41,
                    "100446": 30,
                    "100444": 11,
                    "100443": 33,
                    "100381": 14,
                    "100379": 2,
                    "100377": 10,
                    "100376": 11,
                    "100375": 82
                },
                "generic": {
                    "82": 92197
                }
            },
            "active": {
                "max_price": 4300.07,
                "min_price": 0.16,
                "brand": [
                    100579,
                    100544,
                    100446,
                    100444,
                    100443,
                    100381,
                    100379,
                    100377,
                    100376,
                    100375
                ],
                "generic": [
                    82
                ]
            },
            "checked": {
                "generic": [],
                "brand": []
            }
        }
    }
}
```
> Example response (200):

```json
{
    "success": true,
    "code": 200,
    "data": {
        "items": [
            {
                "_id": "7936542",
                "_source": {
                    "alternatives": {
                        "oen": [
                            "SKBD-0023028",
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "17265"
                        ],
                        "manual": [
                            "SKBD-0023028"
                        ]
                    }
                }
            },
            {
                "_id": "23511",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "BS7104"
                        ],
                        "oen": [
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "17265",
                            "SKBD-0023028"
                        ],
                        "manual": [
                            "BS7104"
                        ]
                    }
                }
            },
            {
                "_id": "177569",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "C3Y025ABE"
                        ],
                        "oen": [
                            "C3Y025ABE",
                            "BS7104",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "17265",
                            "SKBD-0023028"
                        ],
                        "manual": [
                            "C3Y025ABE"
                        ]
                    }
                }
            },
            {
                "_id": "7499510",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "1815319325"
                        ],
                        "oen": [
                            "1815319325",
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "BS-8734",
                            "92230003",
                            "17265",
                            "SKBD-0023028"
                        ],
                        "manual": [
                            "1815319325"
                        ]
                    }
                }
            },
            {
                "_id": "7567080",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "BS-8734"
                        ],
                        "oen": [
                            "BS-8734",
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "92230003",
                            "17265",
                            "SKBD-0023028"
                        ],
                        "manual": [
                            "BS-8734"
                        ]
                    }
                }
            },
            {
                "_id": "7690221",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "92230003"
                        ],
                        "oen": [
                            "92230003",
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "17265",
                            "SKBD-0023028"
                        ],
                        "altArticleNo2": [
                            "92230003"
                        ],
                        "manual": [
                            "92230003"
                        ]
                    }
                }
            },
            {
                "_id": "7710382",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "17265"
                        ],
                        "oen": [
                            "17265",
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "SKBD-0023028"
                        ],
                        "manual": [
                            "17265"
                        ]
                    }
                }
            },
            {
                "_id": "7216564",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "8120 101117"
                        ],
                        "oen": [
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "C3009V",
                            "319325",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "17265",
                            "SKBD-0023028",
                            "8120 101117C",
                            "82B0878",
                            "61360.10"
                        ],
                        "manual": [
                            "8120 101117"
                        ]
                    }
                }
            },
            {
                "_id": "1995203",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "209325"
                        ],
                        "oen": [
                            "209325",
                            "BS7104",
                            "C3Y025ABE",
                            "C3009V",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "17265",
                            "SKBD-0023028",
                            "8120 101117C",
                            "82B0878",
                            "61360.10"
                        ],
                        "manual": [
                            "209325"
                        ]
                    }
                }
            },
            {
                "_id": "7059928",
                "_source": {
                    "alternatives": {
                        "altArticleNo": [
                            "C3009V"
                        ],
                        "oen": [
                            "C3009V",
                            "BS7104",
                            "C3Y025ABE",
                            "209325",
                            "319325",
                            "8120 101117",
                            "1815319325",
                            "BS-8734",
                            "92230003",
                            "17265",
                            "SKBD-0023028",
                            "8120 101117C",
                            "82B0878",
                            "61360.10"
                        ],
                        "altArticleNo2": [
                            "C3009V"
                        ],
                        "manual": [
                            "C3009V"
                        ]
                    }
                }
            }
        ],
        "paginator": {
            "total": 13,
            "relation": "eq",
            "perPage": 10,
            "currentPage": 1,
            "hasNextPage": true,
            "hasPreviousPage": false,
            "lastPage": 2
        }
    }
}
```

### HTTP Request
`GET /products/autocomplete`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `token` | string |  required  | Access token.
        `language` | string |  required  | Language code, size:2.
        `query` | string |  required  | What to search.
    
<!-- END_a427d583e3851b19bb3bc1044d0b6380 -->


